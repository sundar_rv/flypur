package com.flypur.shop.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flypur.shop.api.response.APIStatus;
import com.flypur.shop.api.response.StatusResponse;
import com.flypur.shop.domain.Mail;
import com.flypur.shop.domain.ProductDetails;
import com.flypur.shop.domain.SearchParam;
import com.flypur.shop.domain.TripDetails;
import com.flypur.shop.domain.User;
import com.flypur.shop.domain.UserToken;
import com.flypur.shop.exception.ApplicationException;
import com.flypur.shop.service.ProductDetailService;
import com.flypur.shop.service.TripDetailService;
import com.flypur.shop.util.APIUtil;
import com.flypur.shop.util.Constants;
import com.flypur.shop.util.ReqMapConstants;

@RestController
@RequestMapping(ReqMapConstants.TRIP)
public class TripReqAPI extends APIUtil {
	
	@Autowired
    private TripDetailService tripDetailService;
    
    @Autowired
    private ProductDetailService productDetailService;
    
    @RequestMapping(path = ReqMapConstants.TRIP_ADD, 
    		method = RequestMethod.POST, 
    		produces = ReqMapConstants.CHARSET)
    public String addTrip(@RequestParam String token, @RequestBody TripDetails tripDetails) {
    	logger.debug("Transaction Name = Add Trip, " + tripDetails.getLoginId());
    	statusResponse = new StatusResponse<Object>(APIStatus.TRIP_ADD_INVALID);
    	User user = isValidRequest(token);
    	if(user !=null) {
	    	if(isValidTrip(tripDetails)) {
	    		tripDetails = tripDetailService.saveTrip(tripDetails);
	            statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), tripDetails);
	            sendMail(user, tripDetails);
	            logger.debug("Transaction Name = Add Trip, Status = SUCCESS, " + tripDetails.getLoginId());
	    	} else {
	    		logger.debug("Transaction Name = Add Trip, Status = FAILURE, " + tripDetails.getLoginId());
	    		statusResponse = new StatusResponse<Object>(APIStatus.TRIP_EXISTS);
	    	}
    	}
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.TRIP_GET, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String getTrip(@RequestParam String token, @RequestParam String tripId) {
    	logger.debug("Transaction Name = Get Trip, " + tripId);
    	statusResponse = new StatusResponse<Object>(APIStatus.TRIP_DOESNT_EXISTS);
    	User user = isValidRequest(token);
        if (user != null) {
        	TripDetails tripDetails = tripDetailService.getTripById(Integer.parseInt(tripId), user.getLoginId());
        	if(tripDetails!=null) {
        		statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), tripDetails);
                logger.debug("Transaction Name = Get Trip, Status = SUCCESS, " + tripDetails.getLoginId());
        	}
        }
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.TRIP_GET_SEARCH, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String getTrips(@RequestParam String token, @RequestParam String searchBy, @RequestParam String searchValue) {
    	logger.debug("Transaction Name = Get Trips, " + token);
    	statusResponse = new StatusResponse<Object>(APIStatus.TRIP_SEARCH_INVALID);
    	User user = isValidRequest(token);
    	if(user !=null && searchBy!=null) {
    		if(Constants.TRIP_SEARCH.LOGINID.getValue().equals(searchBy)) {
    			List<TripDetails> tripDetails = tripDetailService.getTripByLoginId(user.getLoginId());
            	if(tripDetails!=null) {
            		statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), tripDetails);
                    logger.debug("Transaction Name = Get Trips, Status = SUCCESS, " + token);
            	} else {
            		statusResponse = new StatusResponse<Object>(APIStatus.TRIP_DOESNT_EXISTS);
            	}
    		} else if (Constants.TRIP_SEARCH.COUNTRY.getValue().equals(searchBy)) {
    			List<TripDetails> tripDetails = tripDetailService.getTripByCountry(searchValue, user.getLoginId());
            	if(tripDetails!=null) {
            		statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), tripDetails);
                    logger.debug("Transaction Name = Get Trips, Status = SUCCESS, " + token);
            	} else {
            		statusResponse = new StatusResponse<Object>(APIStatus.TRIP_DOESNT_EXISTS);
            	}
    		}
    	}
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.TRIP_UPDATE, 
    		method = RequestMethod.POST, 
    		produces = ReqMapConstants.CHARSET)
    public String updateTrip(@RequestParam String token, @RequestBody TripDetails tripDetails) {
    	logger.debug("Transaction Name = Update Trip, " + tripDetails.getLoginId());
    	statusResponse = new StatusResponse<Object>(APIStatus.TRIP_DOESNT_EXISTS);
    	User user = isValidRequest(token);
    	if(user !=null) {
    		TripDetails tripExists = tripDetailService.getTripById(tripDetails.getTripId(), user.getLoginId());
        	if(tripExists!=null) {
        		tripDetails = tripDetailService.saveTrip(tripDetails);
	            statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), tripDetails);
	            logger.debug("Transaction Name = Update Trip, Status = SUCCESS, " + tripDetails.getLoginId());
	    	} else {
	    		logger.debug("Transaction Name = Update Trip, Status = FAILURE, " + tripDetails.getLoginId());
	    		statusResponse = new StatusResponse<Object>(APIStatus.TRIP_DOESNT_EXISTS);
	    	}
    	}
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.PRODUCT_SEARCH, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String searchProducts(@RequestParam String token, @RequestParam String searchParamStr) {
    	logger.debug("Transaction Name = Search Products, " + token);
    	statusResponse = new StatusResponse<Object>(APIStatus.PRODUCT_SEARCH_NODATA);
    	User user = isValidRequest(token);
    	if(user !=null && searchParamStr!=null) {
    		SearchParam searchParam = new SearchParam(searchParamStr);
    		if(CollectionUtils.isNotEmpty(searchParam.getKeyValue())) {
    			List<ProductDetails> buyDetails = productDetailService.productSearch(searchParam);
        		if(CollectionUtils.isNotEmpty(buyDetails)) {
            		statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), buyDetails);
                    logger.debug("Transaction Name = Search Products, Status = SUCCESS, " + token);
            	} else {
            		statusResponse = new StatusResponse<Object>(APIStatus.PRODUCT_SEARCH_NODATA);
            	}
    		} else {
    			statusResponse = new StatusResponse<Object>(APIStatus.PRODUCT_SEARCH_INVALID);
    		}
    	}
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.TRIP_DELETE, 
    		method = RequestMethod.POST, 
    		produces = ReqMapConstants.CHARSET)
    public String deleteTrip(@RequestParam String token, @RequestParam String tripId) {
    	logger.debug("Transaction Name = Delete Trip, " + tripId);
    	statusResponse = new StatusResponse<Object>(APIStatus.TRIP_DOESNT_EXISTS);
    	User user = isValidRequest(token);
    	if(user !=null) {
    		TripDetails tripExists = tripDetailService.getTripById(Integer.parseInt(tripId), user.getLoginId());
        	if(tripExists!=null) {
	    		tripDetailService.deleteTrip(tripExists);
	            statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), "Trip Deleted");
	            logger.debug("Transaction Name = Delete Trip, Status = SUCCESS, " + tripId);
	    	}
    	}
        return writeObjectToJson(statusResponse);
    }
    
    private User isValidRequest(String token) {
    	User user = null;
    	statusResponse = new StatusResponse<Object>(APIStatus.TOKEN_EXPIRIED);
    	UserToken userToken = userTokenService.getTokenById(token);
    	if(userToken!=null) {
    		user = userService.getUserByLoginId(userToken.getLoginId());
            if (user == null) {
            	statusResponse = new StatusResponse<Object>(APIStatus.USER_NOT_ACTIVE);
            }
    	} else {
    		statusResponse = new StatusResponse<Object>(APIStatus.TOKEN_EXPIRIED);
    	}
    	return user;
	}

	private boolean isValidTrip(TripDetails currentTrip) {
    	boolean result = true;
    	List<TripDetails> existingTrips = tripDetailService.getTripByLoginId(currentTrip.getLoginId());
    	for(TripDetails trip : existingTrips) {
    		if(currentTrip.getCountryCde().equals(trip.getCountryCde()) && 
    				currentTrip.getDateRetrn().equals(trip.getDateRetrn())) {
    			result = false;
    			break;
    		}
    	}
    	return result;
    }
    
    private void sendMail(User user, TripDetails tripDetails) {
    	try {
    		Map<String, Object> model = new HashMap<>();
    		model.put("name", user.getFirstName());
    		model.put("userEmail", user.getEmail());
    		model.put("country",tripDetails.getCountryCde());
    		model.put("tripdate",tripDetails.getDateRetrn());
    		model.put("loginId", user.getLoginId());
    		Mail mail = new Mail(user.getEmail(), appConfig.getValueOfKey("mail.subject.addTrip"), model);
            emailService.sendSimpleMessage(mail, "addtrip");
    	} catch (MessagingException | IOException ex) {
    		throw new ApplicationException(ex);
    	}
    }
}
