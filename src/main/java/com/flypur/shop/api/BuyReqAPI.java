package com.flypur.shop.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flypur.shop.api.response.APIStatus;
import com.flypur.shop.api.response.StatusResponse;
import com.flypur.shop.domain.BuyDetails;
import com.flypur.shop.domain.Mail;
import com.flypur.shop.domain.ProductDetails;
import com.flypur.shop.domain.SearchParam;
import com.flypur.shop.domain.User;
import com.flypur.shop.domain.UserToken;
import com.flypur.shop.exception.ApplicationException;
import com.flypur.shop.service.BuyDetailService;
import com.flypur.shop.service.ProductDetailService;
import com.flypur.shop.util.APIUtil;
import com.flypur.shop.util.ReqMapConstants;

@RestController
@RequestMapping(ReqMapConstants.BUY)
public class BuyReqAPI extends APIUtil {
	
    @Autowired
    private BuyDetailService buyDetailService;
    
    @Autowired
    private ProductDetailService productDetailService;
    
    @RequestMapping(path = ReqMapConstants.BUY_ADD, 
    		method = RequestMethod.POST, 
    		produces = ReqMapConstants.CHARSET)
    public String addBuy(@RequestParam String token, @RequestBody BuyDetails buyDetails) {
    	logger.debug("Transaction Name = Add Buy, " + buyDetails.getLoginId());
    	statusResponse = new StatusResponse<Object>(APIStatus.BUY_ADD_INVALID);
    	User user = isValidRequest(token);
    	if(user !=null) {
    		BuyDetails result = buyDetailService.saveBuy(buyDetails);
            statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), result);
            sendMail(user, result);
            logger.debug("Transaction Name = Add Buy, Status = SUCCESS, " + result.getLoginId());
    	}
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.BUY_GET, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String getBuy(@RequestParam String token, @RequestParam String buyId) {
    	logger.debug("Transaction Name = Get Buy, " + buyId);
    	statusResponse = new StatusResponse<Object>(APIStatus.BUY_DOESNT_EXISTS);
    	User user = isValidRequest(token);
        if (user != null) {
        	BuyDetails buyDetails = buyDetailService.getBuyById(user.getLoginId(), Integer.parseInt(buyId));
        	if(buyDetails!=null) {
        		statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), buyDetails);
                logger.debug("Transaction Name = Get Buy, Status = SUCCESS, " + buyDetails.getLoginId());
        	}
        }
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.BUY_GET_SEARCH, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String searchBuyReq(@RequestParam String token, @RequestParam String searchParamStr) {
    	logger.debug("Transaction Name = Search Buys, " + token);
    	statusResponse = new StatusResponse<Object>(APIStatus.BUY_DOESNT_EXISTS);
    	User user = isValidRequest(token);
    	if(user !=null && searchParamStr!=null) {
    		SearchParam searchParam = new SearchParam(searchParamStr);
    		if(CollectionUtils.isNotEmpty(searchParam.getKeyValue())) {
    			List<BuyDetails> buyDetails = buyDetailService.searchBuyReq(searchParam, user.getLoginId());
        		if(CollectionUtils.isNotEmpty(buyDetails)) {
            		statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), buyDetails);
                    logger.debug("Transaction Name = Search Buys, Status = SUCCESS, " + token);
            	} else {
            		statusResponse = new StatusResponse<Object>(APIStatus.BUY_DOESNT_EXISTS);
            	}
    		} else {
    			statusResponse = new StatusResponse<Object>(APIStatus.BUY_DOESNT_EXISTS);
    		}
    	}
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.PRODUCT_SEARCH, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String searchProducts(@RequestParam String token, @RequestParam String searchParamStr) {
    	logger.debug("Transaction Name = Search Products, " + token);
    	statusResponse = new StatusResponse<Object>(APIStatus.PRODUCT_SEARCH_NODATA);
    	User user = isValidRequest(token);
    	if(user !=null && searchParamStr!=null) {
    		SearchParam searchParam = new SearchParam(searchParamStr);
    		if(CollectionUtils.isNotEmpty(searchParam.getKeyValue())) {
    			List<ProductDetails> buyDetails = productDetailService.productSearch(searchParam);
        		if(CollectionUtils.isNotEmpty(buyDetails)) {
            		statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), buyDetails);
                    logger.debug("Transaction Name = Search Products, Status = SUCCESS, " + token);
            	} else {
            		statusResponse = new StatusResponse<Object>(APIStatus.PRODUCT_SEARCH_NODATA);
            	}
    		} else {
    			statusResponse = new StatusResponse<Object>(APIStatus.PRODUCT_SEARCH_INVALID);
    		}
    	}
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.BUY_UPDATE, 
    		method = RequestMethod.POST, 
    		produces = ReqMapConstants.CHARSET)
    public String updateBuy(@RequestParam String token, @RequestBody BuyDetails buyDetails) {
    	logger.debug("Transaction Name = Update Buy, " + buyDetails.getLoginId());
    	statusResponse = new StatusResponse<Object>(APIStatus.BUY_DOESNT_EXISTS);
    	User user = isValidRequest(token);
    	if(user !=null) {
    		BuyDetails tripExists = buyDetailService.getBuyById(user.getLoginId(), buyDetails.getBuyId());
        	if(tripExists!=null) {
        		buyDetailService.saveBuy(buyDetails);
	            statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), buyDetails);
	            logger.debug("Transaction Name = Update Buy, Status = SUCCESS, " + buyDetails.getLoginId());
	    	} else {
	    		logger.debug("Transaction Name = Update Buy, Status = FAILURE, " + buyDetails.getLoginId());
	    		statusResponse = new StatusResponse<Object>(APIStatus.BUY_DOESNT_EXISTS);
	    	}
    	}
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.BUY_DELETE, 
    		method = RequestMethod.POST, 
    		produces = ReqMapConstants.CHARSET)
    public String deleteBuy(@RequestParam String token, @RequestParam String buyId) {
    	logger.debug("Transaction Name = Delete Buy, " + buyId);
    	statusResponse = new StatusResponse<Object>(APIStatus.BUY_DOESNT_EXISTS);
    	User user = isValidRequest(token);
    	if(user !=null) {
    		BuyDetails tripExists = buyDetailService.getBuyById(user.getLoginId(), Integer.parseInt(buyId));
        	if(tripExists!=null) {
        		buyDetailService.deleteBuy(tripExists);
	            statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), "Buy Deleted");
	            logger.debug("Transaction Name = Delete Buy, Status = SUCCESS, " + buyId);
	    	} else {
	    		logger.debug("Transaction Name = Delete Buy, Status = FAILURE, " + buyId);
	    		statusResponse = new StatusResponse<Object>(APIStatus.BUY_DOESNT_EXISTS);
	    	}
    	}
        return writeObjectToJson(statusResponse);
    }
    
    private User isValidRequest(String token) {
    	User user = null;
    	UserToken userToken = userTokenService.getTokenById(token);
    	if(userToken!=null) {
    		user = userService.getUserByLoginId(userToken.getLoginId());
            if (user == null) {
            	statusResponse = new StatusResponse<Object>(APIStatus.USER_NOT_ACTIVE);
            }
    	} else {
    		statusResponse = new StatusResponse<Object>(APIStatus.TOKEN_EXPIRIED);
    	}
    	return user;
	}

	private void sendMail(User user, BuyDetails buyDetails) {
    	try {
    		Map<String, Object> model = new HashMap<>();
    		model.put("name", user.getFirstName());
    		model.put("userEmail", user.getEmail());
    		model.put("productName",buyDetails.getProductDetails().getProductName());
    		model.put("country",buyDetails.getProductDetails().getCountry().getCountryName());
    		model.put("expdate",buyDetails.getReqExpireDate());
    		model.put("loginId", user.getLoginId());
    		Mail mail = new Mail(user.getEmail(), appConfig.getValueOfKey("mail.subject.addBuy"), model);
            emailService.sendSimpleMessage(mail, "buyregister");
    	} catch (MessagingException | IOException ex) {
    		throw new ApplicationException(ex);
    	}
    }

}
