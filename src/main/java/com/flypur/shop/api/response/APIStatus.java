package com.flypur.shop.api.response;

public enum APIStatus {

    ERR_INVALID_DATA(100, "Input data is not valid."),
    ERR_USER_NOT_EXIST(110, "User does not exist"),
    ERR_USER_NOT_VALID(111, "User name or password is not correct"),
    USER_ALREADY_EXIST(112, "Email already exist"),
    USER_PENDING_STATUS(113, "User have not activated"),
    USER_DOESNT_EXIST(114, "User doesn't exist"),
    USER_NOT_ACTIVE(115, "User not activated"),
    INVALID_PARAMETER(200, "Invalid request parameter"),
    TOKEN_EXPIRIED(202, "Token expiried"),
    REQUIRED_LOGIN(203, "Required login"),
    INVALID_TOKEN(204, "Invalid token"),
    PASS_INVALID(205, "Password less than 6 digits"),
    // Common status
    OK(200, null),
    //Client Errors
	BAD(400, "Bad Request"),
	UNAUTH(401, "Unauthorized Request"),
	NOTFOUND(404, "Request not found"),
	//Server Errors
	NOTIMPL(501, "Request Not Implemented"),
	BADGTW(502, "Bad Gateway"),
	SVCUNAVAIL(503, "Service Unavailable"),
	GTWTIMOT(504, "Gateway Timeout"),
	
	//Trip Errors
	TRIP_ADD_INVALID(300, "Add Trip Invalid"),
	TRIP_EXISTS(301, "Trip exists on the same date"),
	TRIP_DOESNT_EXISTS(302, "Trip does not exists"),
	TRIP_SEARCH_INVALID(303, "Invalid Search Trip criteria"),
	PRODUCT_SEARCH_INVALID(304, "Invalid Search Product criteria"), 
	PRODUCT_SEARCH_NODATA(305, "No Products available"),
	
	//Buy Errors
	BUY_ADD_INVALID(350, "Add Buy Invalid"),
	BUY_DOESNT_EXISTS(351, "Buy does not exists"),
	BUY_SEARCH_INVALID(352, "Invalid Search Buy criteria"),
	
	//Manage order Errors
	MANAGE_ORDER_TRIP_INVALID(360, "Invalid Trip details"),
	MANAGE_ORDER_BUY_INVALID(361, "Invalid Buy details"),
	MANAGE_ORDER_BUY_EXPIRED(362, "Buy request expired"),
	MANAGE_ORDER_TRIP_EXPIRED(363, "Trip request expired"),
	MANAGE_ORDER_TRIP_BUY_CNTRY_NOMATCH(364, "Trip & Buy request doesn't match for Country"),
	MANAGE_ORDER_SEARCH_INVALID(365, "Invalid Search criteria"),
	MANAGE_ORDER_SEARCH_NODATA(366, "No Orders available"),
	MANAGE_ORDER_REMOVED(367, "Order removed")
	;

    private final int code;
    private final String description;

    private APIStatus(int s, String v) {
        code = s;
        description = v;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}
