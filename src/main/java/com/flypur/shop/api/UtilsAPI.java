package com.flypur.shop.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flypur.shop.api.response.APIStatus;
import com.flypur.shop.api.response.StatusResponse;
import com.flypur.shop.domain.CollectionMethod;
import com.flypur.shop.domain.Country;
import com.flypur.shop.domain.ProductCategory;
import com.flypur.shop.domain.Status;
import com.flypur.shop.service.UtilService;
import com.flypur.shop.util.APIUtil;
import com.flypur.shop.util.ReqMapConstants;

@RestController
@RequestMapping(ReqMapConstants.UTILS)
public class UtilsAPI extends APIUtil {
	
	@Autowired
    private UtilService utilService;
	
	@RequestMapping(path = ReqMapConstants.UTIL_GET_ALL_CNTRYLIST, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String getAllCountries() {
    	logger.debug("Transaction Name = Get All Countries");
    	List<Country> cntryLst = utilService.getAllCountries();
    	if(cntryLst!=null) {
    		statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), cntryLst);
    	}
        return writeObjectToJson(statusResponse);
    }
	
	@RequestMapping(path = ReqMapConstants.UTIL_GET_CNTRY_NAME, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String getCountryName(@RequestParam String countryCde) {
    	logger.debug("Transaction Name = Get Country Name, " + countryCde);
        Country cntryLst = utilService.getCountryName(countryCde);
    	if(cntryLst!=null) {
    		statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), cntryLst);
    	}
        return writeObjectToJson(statusResponse);
    }
	
	@RequestMapping(path = ReqMapConstants.UTIL_GET_ALL_STATUSLIST, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String getAllStatus() {
    	logger.debug("Transaction Name = Get All Status");
        List<Status> statusLst = utilService.getAllStatus();
    	if(statusLst!=null) {
    		statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), statusLst);
    	}
        return writeObjectToJson(statusResponse);
    }
	
	@RequestMapping(path = ReqMapConstants.UTIL_GET_STATUS_NAME, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String getStatusName(@RequestParam String statusId) {
		logger.debug("Transaction Name = Get Status Name, " + statusId);
        Status status = utilService.getStatusName(Integer.parseInt(statusId));
    	if(status!=null) {
    		statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), status);
    	}
        return writeObjectToJson(statusResponse);
    }
	
	@RequestMapping(path = ReqMapConstants.UTIL_GET_ALL_CTGRYLIST, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String getAllCategories() {
		logger.debug("Transaction Name = Get All Category");
        List<ProductCategory> categoryLst = utilService.getAllCategories();
    	if(categoryLst!=null) {
    		statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), categoryLst);
    	}
        return writeObjectToJson(statusResponse);
    }
	
	@RequestMapping(path = ReqMapConstants.UTIL_GET_CTGRY_NAME, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String getCategoryName(@RequestParam String ctgryId) {
		logger.debug("Transaction Name = Get Status Name, " + ctgryId);
		ProductCategory category = utilService.getCategoryName(Integer.parseInt(ctgryId));
    	if(category!=null) {
    		statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), category);
    	}
        return writeObjectToJson(statusResponse);
    }
	
	@RequestMapping(path = ReqMapConstants.UTIL_GET_ALL_COLLLIST, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String getAllCollectionMethods() {
		logger.debug("Transaction Name = Get All Collection Methods");
        List<CollectionMethod> collectionMethodLst = utilService.getCollectionMethods();
    	if(collectionMethodLst!=null) {
    		statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), collectionMethodLst);
    	}
        return writeObjectToJson(statusResponse);
    }
	
	@RequestMapping(path = ReqMapConstants.UTIL_GET_COLL_NAME, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String getCollMethodName(@RequestParam String collectionMethodId) {
		logger.debug("Transaction Name = Get Collection Method Name, " + collectionMethodId);
		CollectionMethod collMethod = utilService.getCollectionMethodName(Integer.parseInt(collectionMethodId));
    	if(collMethod!=null) {
    		statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), collMethod);
    	}
        return writeObjectToJson(statusResponse);
    }

}
