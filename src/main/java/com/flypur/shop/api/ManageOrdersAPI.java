package com.flypur.shop.api;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flypur.shop.api.response.APIStatus;
import com.flypur.shop.api.response.StatusResponse;
import com.flypur.shop.domain.BuyDetails;
import com.flypur.shop.domain.Mail;
import com.flypur.shop.domain.ManageOrder;
import com.flypur.shop.domain.SearchParam;
import com.flypur.shop.domain.TripDetails;
import com.flypur.shop.domain.User;
import com.flypur.shop.domain.UserToken;
import com.flypur.shop.exception.ApplicationException;
import com.flypur.shop.service.BuyDetailService;
import com.flypur.shop.service.ManageOrderService;
import com.flypur.shop.service.TripDetailService;
import com.flypur.shop.util.APIUtil;
import com.flypur.shop.util.ReqMapConstants;

@RestController
@RequestMapping(ReqMapConstants.MANAGE)
public class ManageOrdersAPI extends APIUtil {
	
	private static final String ORDER_ADD = "ORDER_ADD";
	private static final String ORDER_REMOVE = "ORDER_REMOVE";
	private static final String ORDER_UPDATE = "ORDER_UPDATE";

	@Autowired
    private BuyDetailService buyDetailService;
    
    @Autowired
    private TripDetailService tripDetailService;
    
    @Autowired
    private ManageOrderService manageOrderService;
    
    @RequestMapping(path = ReqMapConstants.PREORDER_ADD, 
    		method = RequestMethod.POST, 
    		produces = ReqMapConstants.CHARSET)
    public String addPreOrder(@RequestParam String token, @RequestBody ManageOrder manageOrder) {
    	logger.debug("Transaction Name = Add PreOrder, " + manageOrder.getLoginId());
    	statusResponse = new StatusResponse<Object>(APIStatus.ERR_INVALID_DATA);
    	User user = isValidRequest(token);
    	if(user !=null) {
	    	if(isValidOrder(manageOrder)) {
	    		manageOrder = manageOrderService.addPreOrder(manageOrder);
	            statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), manageOrder);
	            logger.debug("Transaction Name = Add PreOrder, Status = SUCCESS, " + manageOrder.getLoginId());
	    	} else {
	    		logger.debug("Transaction Name = Add PreOrder, Status = FAILURE, " + manageOrder.getLoginId());
	    	}
    	}
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.PREORDER_UPDATE_STATUS, 
    		method = RequestMethod.POST, 
    		produces = ReqMapConstants.CHARSET)
    public String updatePreOrderStatus(@RequestParam String token, @RequestBody ManageOrder manageOrder) {
    	logger.debug("Transaction Name = Update PreOrder Status, " + manageOrder.getLoginId());
    	statusResponse = new StatusResponse<Object>(APIStatus.ERR_INVALID_DATA);
    	User user = isValidRequest(token);
    	if(user !=null) {
    		boolean validOrder = false;
    		ManageOrder manageOrderExists = manageOrderService.findPreOrder(manageOrder.getOrderId());
    		if((manageOrder.getStatus().getStatusId() == 2 || manageOrder.getStatus().getStatusId() == 15) && 
    				user.getLoginId().equals(manageOrderExists.getTripDetails().getLoginId())) {
    			validOrder = true;
    		} else if((manageOrder.getStatus().getStatusId() == 3 || manageOrder.getStatus().getStatusId() == 14) && 
    				user.getLoginId().equals(manageOrderExists.getBuyDetails().getLoginId())) {
    			validOrder = true;
    		}
	    	if(validOrder) {
	    		manageOrder = manageOrderService.updatePreOrderStatus(manageOrder);
	            statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), manageOrder);
	            logger.debug("Transaction Name = Add PreOrder Status, Status = SUCCESS, " + manageOrder.getLoginId());
	    	} else {
	    		logger.debug("Transaction Name = Add PreOrder Status, Status = FAILURE, " + manageOrder.getLoginId());
	    	}
    	}
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.ORDER_UPDATE_STATUS, 
    		method = RequestMethod.POST, 
    		produces = ReqMapConstants.CHARSET)
    public String updateOrderStatus(@RequestParam String token, @RequestBody ManageOrder manageOrder) {
    	logger.debug("Transaction Name = Update Order Status, " + manageOrder.getLoginId());
    	statusResponse = new StatusResponse<Object>(APIStatus.ERR_INVALID_DATA);
    	User user = isValidRequest(token);
    	if(user !=null) {
    		ManageOrder manageOrderExists = manageOrderService.findOrder(manageOrder.getOrderId());
    		if(manageOrderExists!=null && 
    				(manageOrder.getBuyDetails().getLoginId().equals(user.getLoginId()) || manageOrder.getTripDetails().getLoginId().equals(user.getLoginId()))) {
    			manageOrder = manageOrderService.updateOrderStatus(manageOrder);
    			statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), manageOrder);
    			BuyDetails buyDetails = buyDetailService.getBuyById(manageOrder.getBuyDetails().getBuyId());
    			TripDetails tripDetails = tripDetailService.getTripById(manageOrder.getTripDetails().getTripId(), user.getLoginId());
                sendMail1(user, manageOrder, buyDetails, tripDetails, ORDER_UPDATE);
                logger.debug("Transaction Name = Update Order Status, Status = SUCCESS, " + user.getLoginId());
    		} else {
    			statusResponse = new StatusResponse<Object>(APIStatus.MANAGE_ORDER_SEARCH_NODATA);
    		}
    	}
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.ORDER_UPDATE, 
    		method = RequestMethod.POST, 
    		produces = ReqMapConstants.CHARSET)
    public String updateOrder(@RequestParam String token, @RequestBody ManageOrder manageOrder) {
    	logger.debug("Transaction Name = Update Orders, " + manageOrder.getLoginId());
    	statusResponse = new StatusResponse<Object>(APIStatus.ERR_INVALID_DATA);
    	User user = isValidRequest(token);
    	if(user !=null) {
    		ManageOrder manageOrderExists = manageOrderService.findOrder(manageOrder.getOrderId());
    		if(manageOrderExists!=null && manageOrder.getLoginId().equals(user.getLoginId())) {
    			manageOrder = manageOrderService.updateOrder(user, manageOrder);
    			statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), manageOrder);
    			BuyDetails buyDetails = buyDetailService.getBuyById(manageOrder.getBuyDetails().getBuyId());
    			TripDetails tripDetails = tripDetailService.getTripById(manageOrder.getTripDetails().getTripId(), user.getLoginId());
                sendMail1(user, manageOrder, buyDetails, tripDetails, ORDER_UPDATE);
                logger.debug("Transaction Name = Update Orders, Status = SUCCESS, " + user.getLoginId());
    		} else {
    			statusResponse = new StatusResponse<Object>(APIStatus.MANAGE_ORDER_SEARCH_NODATA);
    		}
    	}
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.GET_ORDER, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String getOrder(@RequestParam String token, @RequestParam String orderId) {
    	statusResponse = new StatusResponse<Object>(APIStatus.ERR_INVALID_DATA);
    	User user = isValidRequest(token);
    	logger.debug("Transaction Name = Get Order, " + user.getLoginId());
    	if(user !=null) {
	    	ManageOrder manageOrder = manageOrderService.findOrder(Integer.parseInt(orderId));
            statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), manageOrder);
            logger.debug("Transaction Name = Get Order, Status = SUCCESS, " + manageOrder.getLoginId());
    	}
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.GET_ORDERS, 
    		method = RequestMethod.GET, 
    		produces = ReqMapConstants.CHARSET)
    public String getOrders(@RequestParam String token, @RequestParam String searchParamStr) {
    	logger.debug("Transaction Name = Get Orders, " + token);
    	statusResponse = new StatusResponse<Object>(APIStatus.ERR_INVALID_DATA);
    	User user = isValidRequest(token);
    	logger.debug("Transaction Name = Get Orders, " + user.getLoginId());
    	if(user !=null && searchParamStr!=null) {
    		SearchParam searchParam = new SearchParam(searchParamStr);
    		if(CollectionUtils.isNotEmpty(searchParam.getKeyValue())) {
    			List<ManageOrder> manageOrderLst = manageOrderService.findOrders(searchParam);
    			if(CollectionUtils.isNotEmpty(manageOrderLst)) {
    				statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), manageOrderLst);
    			} else {
    				statusResponse = new StatusResponse<Object>(APIStatus.MANAGE_ORDER_SEARCH_NODATA);
    			}
                logger.debug("Transaction Name = Get Orders, Status = SUCCESS, " + user.getLoginId());
    		} else {
    			statusResponse = new StatusResponse<Object>(APIStatus.MANAGE_ORDER_SEARCH_INVALID);
    		}
    	}
        return writeObjectToJson(statusResponse);
    }
    
    private User isValidRequest(String token) {
    	User user = null;
    	UserToken userToken = userTokenService.getTokenById(token);
    	if(userToken!=null) {
    		user = userService.getUserByLoginId(userToken.getLoginId());
            if (user == null) {
            	statusResponse = new StatusResponse<Object>(APIStatus.USER_NOT_ACTIVE);
            }
    	} else {
    		statusResponse = new StatusResponse<Object>(APIStatus.TOKEN_EXPIRIED);
    	}
    	return user;
	}
    
    private boolean isValidOrder(ManageOrder manageOrder) {
    	boolean result = false;
    	TripDetails existingTrip = tripDetailService.getTripById(manageOrder.getTripDetails().getTripId(), 
    			manageOrder.getLoginId());
    	manageOrder.setTripDetails(existingTrip);
    	BuyDetails buyReq = buyDetailService.getBuyById(manageOrder.getBuyDetails().getBuyId());
    	buyReq.setOfferDetails(manageOrder.getBuyDetails().getOfferDetails());
    	manageOrder.setBuyDetails(buyReq);
    	if (existingTrip==null || buyReq == null) {
    		statusResponse = new StatusResponse<Object>(APIStatus.ERR_INVALID_DATA);
    	} else if(existingTrip.getDateRetrn().toDate().before(new Date())) {
    		statusResponse = new StatusResponse<Object>(APIStatus.MANAGE_ORDER_TRIP_EXPIRED);
    	} else if(buyReq.getReqExpireDate().toDate().before(new Date())) {
    		statusResponse = new StatusResponse<Object>(APIStatus.MANAGE_ORDER_BUY_EXPIRED);
    	} else if (!existingTrip.getCountryCde().equals(buyReq.getProductDetails().getCountry().getCountryCde())){
    		statusResponse = new StatusResponse<Object>(APIStatus.MANAGE_ORDER_TRIP_BUY_CNTRY_NOMATCH);
    	} else if (buyReq.getStatusId()==3){
    		statusResponse = new StatusResponse<Object>(APIStatus.MANAGE_ORDER_BUY_EXPIRED);
    	} else {
    		result = true;
    	}
    	return result;
    }
    
    private void sendMail1(User user, ManageOrder manageOrder, BuyDetails buyDetails, 
    		TripDetails tripDetails, String mailText) {
    	try {
    		Map<String, Object> model = new HashMap<>();
    		model.put("name", user.getFirstName());
    		model.put("userEmail", user.getEmail());
    		model.put("productName",buyDetails.getProductDetails().getProductName());
    		model.put("country",buyDetails.getProductDetails().getCountry().getCountryName());
    		model.put("expdate",buyDetails.getReqExpireDate());
    		model.put("loginId", user.getLoginId());
    		Mail mail = null;
    		if(ORDER_ADD.equals(mailText)) {
    			
    			mail = new Mail(user.getEmail(), appConfig.getValueOfKey("mail.subject.AddOrder"), model);
                emailService.sendSimpleMessage(mail, "orderregister");
    		} else if (ORDER_REMOVE.equals(mailText)) {
    			mail = new Mail(user.getEmail(), appConfig.getValueOfKey("mail.subject.RemoveOrder"), model);
                emailService.sendSimpleMessage(mail, "orderderegister");
    		} else if (ORDER_UPDATE.equals(mailText)) {
    			mail = new Mail(user.getEmail(), appConfig.getValueOfKey("mail.subject.UpdateOrder"), model);
                emailService.sendSimpleMessage(mail, "orderupdate");
    		}
    	} catch (MessagingException | IOException ex) {
    		logger.error("ManageOrder - Error in sending email, " + ex.getMessage());
    		throw new ApplicationException(ex);
    	}
		
	}
}
