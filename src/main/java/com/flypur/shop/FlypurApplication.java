package com.flypur.shop;

import static springfox.documentation.builders.PathSelectors.regex;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@ComponentScan(basePackages = "com.flypur.shop")
@EnableSwagger2
@SpringBootApplication
public class FlypurApplication {
	
	private static final Logger logger = LogManager.getLogger(FlypurApplication.class.getName());

	public static void main(String[] args) {
		logger.info("Application Starting...");
		SpringApplication.run(FlypurApplication.class, args);
	}
	
	@Bean
    public Docket newsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("apis")
                .apiInfo(apiInfo())
                .select()
                .paths(regex("/api.*"))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Flypur REST API Documents")
                .description("Documents with Swagger 2")
                .termsOfServiceUrl("http://flypur.com")
                .contact(new Contact("Flypur", "http://flypur.com", "contact@flypur.com"))
                .license("")
                .licenseUrl("")
                .version("1.0")
                .build();
    }
}
