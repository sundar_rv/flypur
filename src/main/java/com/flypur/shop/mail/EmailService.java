package com.flypur.shop.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.flypur.shop.config.AppConfig;
import com.flypur.shop.domain.Mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Service
public class EmailService {

    @Autowired
    private JavaMailSenderImpl emailSender;

    @Autowired
    private SpringTemplateEngine templateEngine;
    
    @Autowired
    private AppConfig appConfig;


    public void sendSimpleMessage(Mail mail, String template) throws MessagingException, IOException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
        Context context = new Context();
        context.setVariables(mail.getModel());
        String html = templateEngine.process(template, context);
        
        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(appConfig.getValueOfKey("spring.mail.username"));
        helper.addInline("mailtop", new ClassPathResource("/templates/mail/mailtop.png"));
        helper.addInline("leftborder", new ClassPathResource("/templates/mail/leftborder.png"));
        helper.addInline("flypurmaillogo", new ClassPathResource("/templates/mail/logo.png"));
        helper.addInline("rightborder", new ClassPathResource("/templates/mail/rightborder.png"));
        helper.addInline("mailbottom", new ClassPathResource("/templates/mail/mailbottom.png"));
        emailSender.send(message);
    }
    
    public void sendMessageWithAttach(Mail mail, String template, File attachFile) 
    		throws MessagingException, IOException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());
        Context context = new Context();
        context.setVariables(mail.getModel());
        String html = templateEngine.process(template, context);
        
        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(appConfig.getValueOfKey("spring.mail.username"));
        helper.addInline("flypurmaillogo", new ClassPathResource("/templates/mail/logo.png"));
        helper.addAttachment(attachFile.getName(), attachFile);
        emailSender.send(message);
    }
}
