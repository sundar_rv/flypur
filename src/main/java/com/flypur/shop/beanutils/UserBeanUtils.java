package com.flypur.shop.beanutils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.LocalDate;

import com.flypur.shop.domain.User;
import com.flypur.shop.domain.UserAddress;
import com.flypur.shop.domain.UserToken;
import com.flypur.shop.repository.model.UserAddressRepo;
import com.flypur.shop.repository.model.UserRepo;
import com.flypur.shop.repository.model.UserTokenRepo;

public final class UserBeanUtils {
	
	public static UserRepo getUserRepo(User user) {
		UserRepo userRepo = null;
		if(user!=null) {
			userRepo = new UserRepo();
			userRepo.setLoginId(user.getLoginId());
			userRepo.setRoleId(user.getRoleId());
			userRepo.setEmail(user.getEmail());
			userRepo.setPasswordHash(user.getPasswordHash());
			userRepo.setFirstName(user.getFirstName());
			userRepo.setMiddleName(user.getMiddleName());
			userRepo.setLastName(user.getLastName());
			userRepo.setStatus(user.getStatus());
			userRepo.setSalt(user.getSalt());
			userRepo.setCreateDate(user.getCreateDate());
		}
		return userRepo;
	}
	
	public static List<UserAddressRepo> getUserAddressRepo(User user) {
		List<UserAddressRepo> userAddressRepoLst = new ArrayList<UserAddressRepo>();
		if(user!=null && CollectionUtils.isNotEmpty(user.getUserAddressLst())) {
			for(UserAddress userAddress : user.getUserAddressLst()) {
				UserAddressRepo userAddressRepo = new UserAddressRepo();
				userAddressRepo.setLoginId(userAddress.getLoginId());
				userAddressRepo.setAddressId(userAddress.getAddressId());
				userAddressRepo.setAddressLine1(userAddress.getAddressLine1());
				userAddressRepo.setAddressLine2(userAddress.getAddressLine2());
				userAddressRepo.setCity(userAddress.getCity());
				userAddressRepo.setCountry(userAddress.getCountry());
				userAddressRepo.setPincode(userAddress.getPincode());
				userAddressRepo.setPhone(userAddress.getPhone());
				userAddressRepo.setMobile(userAddress.getMobile());
				userAddressRepoLst.add(userAddressRepo);
			}
		}
		return userAddressRepoLst;			
	}
	
	public static UserAddressRepo getUserAddressRepo(UserAddress userAddress) {
		UserAddressRepo userAddressRepo = null;
		if(userAddress!=null) {
			userAddressRepo = new UserAddressRepo();
			userAddressRepo.setLoginId(userAddress.getLoginId());
			userAddressRepo.setAddressId(userAddress.getAddressId());
			userAddressRepo.setAddressLine1(userAddress.getAddressLine1());
			userAddressRepo.setAddressLine2(userAddress.getAddressLine2());
			userAddressRepo.setCity(userAddress.getCity());
			userAddressRepo.setCountry(userAddress.getCountry());
			userAddressRepo.setPincode(userAddress.getPincode());
			userAddressRepo.setPhone(userAddress.getPhone());
			userAddressRepo.setMobile(userAddress.getMobile());
		}
		return userAddressRepo;			
	}
	
	public static User getUser(UserRepo userRepo, List<UserAddressRepo> userAddressRepoLst) {
		User user = null;
		if(userRepo != null) {
			user = new User();
			user.setLoginId(userRepo.getLoginId());
			user.setRoleId(userRepo.getRoleId());
			user.setEmail(userRepo.getEmail());
			user.setPasswordHash(userRepo.getPasswordHash());
			user.setFirstName(userRepo.getFirstName());
			user.setMiddleName(userRepo.getMiddleName());
			user.setLastName(userRepo.getLastName());
			user.setStatus(userRepo.getStatus());
			user.setSalt(userRepo.getSalt());
			user.setCreateDate(userRepo.getCreateDate());
			if (CollectionUtils.isNotEmpty(userAddressRepoLst)) {
				for (UserAddressRepo userAddressRepo : userAddressRepoLst) {
					UserAddress userAddress = new UserAddress();
					userAddress.setLoginId(userAddressRepo.getLoginId());
					userAddress.setAddressId(userAddressRepo.getAddressId());
					userAddress.setAddressLine1(userAddressRepo.getAddressLine1());
					userAddress.setAddressLine2(userAddressRepo.getAddressLine2());
					userAddress.setCity(userAddressRepo.getCity());
					userAddress.setCountry(userAddressRepo.getCountry());
					userAddress.setPincode(userAddressRepo.getPincode());
					userAddress.setPhone(userAddressRepo.getPhone());
					userAddress.setMobile(userAddressRepo.getMobile());
					user.addUserAddress(userAddress);
				}
			}
		}
		return user;
	}
	
	public static UserAddress getUserAddress(UserAddressRepo userAddressRepo) {
		UserAddress userAddress = null;
		if(userAddressRepo!=null) {
			userAddress = new UserAddress();
			userAddress.setLoginId(userAddressRepo.getLoginId());
			userAddress.setAddressId(userAddressRepo.getAddressId());
			userAddress.setAddressLine1(userAddressRepo.getAddressLine1());
			userAddress.setAddressLine2(userAddressRepo.getAddressLine2());
			userAddress.setCity(userAddressRepo.getCity());
			userAddress.setCountry(userAddressRepo.getCountry());
			userAddress.setPincode(userAddressRepo.getPincode());
			userAddress.setPhone(userAddressRepo.getPhone());
			userAddress.setMobile(userAddressRepo.getMobile());
		}
		
		return userAddress;			
	}
	
	public static UserTokenRepo getUserTokenRepo(UserToken userToken) {
		UserTokenRepo userTokenRepo = null;
		if(userToken!=null) {
			userTokenRepo = new UserTokenRepo();
			userTokenRepo.setLoginId(userToken.getLoginId());
			userTokenRepo.setToken(userToken.getToken());
			userTokenRepo.setLoginDate(userToken.getLoginDate().toDate());
			userTokenRepo.setExpirationDate(userToken.getExpirationDate().toDate());
		}
		return userTokenRepo;
	}
	
	public static UserToken getUserToken(UserTokenRepo userTokenRepo) {
		UserToken userToken = null;
		if(userTokenRepo!=null) {
			userToken = new UserToken();
			userToken.setLoginId(userTokenRepo.getLoginId());
			userToken.setToken(userTokenRepo.getToken());
			userToken.setLoginDate(new LocalDate(userTokenRepo.getLoginDate().getTime()));
			userToken.setExpirationDate(new LocalDate(userTokenRepo.getExpirationDate().getTime()));
		}
		
		return userToken;
	}
}
