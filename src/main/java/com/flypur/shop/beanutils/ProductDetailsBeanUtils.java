package com.flypur.shop.beanutils;

import com.flypur.shop.domain.Country;
import com.flypur.shop.domain.ProductCategory;
import com.flypur.shop.domain.ProductDetails;
import com.flypur.shop.repository.model.CountryRepo;
import com.flypur.shop.repository.model.ProductCategoryRepo;
import com.flypur.shop.repository.model.ProductDetailsRepo;

public class ProductDetailsBeanUtils {
	
	private ProductDetailsBeanUtils() {
		
	}
	
	public static ProductDetailsRepo getProductDetailsRepo(ProductDetails productDetails) {
		ProductDetailsRepo productDetailsRepo = new ProductDetailsRepo();
		productDetailsRepo.setProductId(productDetails.getProductId());
		productDetailsRepo.setProductName(productDetails.getProductName());
		productDetailsRepo.setProductSize(productDetails.getProductSize());
		productDetailsRepo.setProductType(productDetails.getProductType());
		productDetailsRepo.setProductUrl(productDetails.getProductUrl());
		productDetailsRepo.setPicture(productDetails.getPicture());
		productDetailsRepo.setCountryCde(productDetails.getCountry().getCountryCde());
		productDetailsRepo.setCategoryId(productDetails.getCategory().getCategoryId());
		return productDetailsRepo;
	}
	
	public static ProductCategoryRepo getProductCategoryRepo(ProductCategory productCategory) {
		ProductCategoryRepo productCategoryRepo = new ProductCategoryRepo();
		productCategoryRepo.setCategoryId(productCategory.getCategoryId());
		productCategoryRepo.setCategoryName(productCategory.getCategoryName());
		productCategoryRepo.setSubCategoryName(productCategory.getSubCategoryName());
		return productCategoryRepo;
	}
	
	public static ProductDetails getProductsDetails(ProductDetailsRepo productDetailsRepo, 
			ProductCategoryRepo productCategoryRepo, CountryRepo countryRepo) {
		ProductDetails productDetails = new ProductDetails();
		productDetails.setProductId(productDetailsRepo.getProductId());
		productDetails.setProductName(productDetailsRepo.getProductName());
		productDetails.setCategory(getCategory(productCategoryRepo));
		productDetails.setPicture(productDetailsRepo.getPicture());
		productDetails.setProductUrl(productDetailsRepo.getProductUrl());
		productDetails.setCountry(getCountryDetails(countryRepo));
		productDetails.setProductType(productDetailsRepo.getProductType());
		productDetails.setProductSize(productDetailsRepo.getProductSize());
		return productDetails;
	}

	private static Country getCountryDetails(CountryRepo countryRepo) {
		Country country = new Country();
		country.setCountryCde(countryRepo.getCountryCde());
		country.setCountryName(countryRepo.getCountryName());
		return country;
	}

	public static ProductCategory getCategory(ProductCategoryRepo productCategoryRepo) {
		ProductCategory productCategory = new ProductCategory();
		productCategory.setCategoryId(productCategoryRepo.getCategoryId());
		productCategory.setCategoryName(productCategoryRepo.getCategoryName());
		productCategory.setSubCategoryName(productCategoryRepo.getSubCategoryName());
		return productCategory;
	}

	

}
