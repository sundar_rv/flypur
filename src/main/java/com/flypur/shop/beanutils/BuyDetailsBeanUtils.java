package com.flypur.shop.beanutils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.flypur.shop.domain.BuyDetails;
import com.flypur.shop.domain.Status;
import com.flypur.shop.repository.model.BuyDetailsRepo;
import com.flypur.shop.repository.model.CountryRepo;
import com.flypur.shop.repository.model.PreOrderRepo;
import com.flypur.shop.repository.model.ProductCategoryRepo;
import com.flypur.shop.repository.model.ProductDetailsRepo;

public final class BuyDetailsBeanUtils {
	
	public static BuyDetailsRepo getBuyDetailsRepo(BuyDetails buyDetails) {
		BuyDetailsRepo buyDetailsRepo = new BuyDetailsRepo();
		buyDetailsRepo.setBuyId(buyDetails.getBuyId());
		buyDetailsRepo.setLoginId(buyDetails.getLoginId());
		buyDetailsRepo.setProductId(buyDetails.getProductDetails().getProductId());
		buyDetailsRepo.setQuantity(buyDetails.getQuantity());
		buyDetailsRepo.setReqExpireDate(buyDetails.getReqExpireDate());
		buyDetailsRepo.setStatusId(buyDetails.getStatusId());
		buyDetailsRepo.setSendNotifications(buyDetails.getSendNotifications());
		buyDetailsRepo.setOrderDate(buyDetails.getOrderDate());
		return buyDetailsRepo;
	}
	
	public static BuyDetails getBuyDetails(BuyDetailsRepo buyDetailsRepo, ProductDetailsRepo productDetailsRepo, 
			ProductCategoryRepo productCategoryRepo, CountryRepo countryRepo, List<PreOrderRepo> preOrderRepoLst, Map<Integer, Status> statusMap) {
		BuyDetails buyDetails = new BuyDetails();
		buyDetails.setBuyId(buyDetailsRepo.getBuyId());
		buyDetails.setLoginId(buyDetailsRepo.getLoginId());
		buyDetails.setProductDetails(ProductDetailsBeanUtils.getProductsDetails(productDetailsRepo, productCategoryRepo, countryRepo));
		buyDetails.setQuantity(buyDetailsRepo.getQuantity());
		buyDetails.setReqExpireDate(buyDetailsRepo.getReqExpireDate());
		buyDetails.setOfferDetails(PreOrderBeanUtils.getPreOrderDetails(preOrderRepoLst, statusMap));
		buyDetails.setStatusId(buyDetailsRepo.getStatusId());
		buyDetails.setSendNotifications(buyDetailsRepo.getSendNotifications());
		buyDetails.setOrderDate(buyDetailsRepo.getOrderDate());
		return buyDetails;
	}	

	public static List<BuyDetails> getBuyDetails(List<BuyDetailsRepo> buyDetailsRepoLst, 
			Map<Integer, ProductDetailsRepo> productDetailsRepoMap, Map<Integer, 
			ProductCategoryRepo> productCategoryRepoMap, Map<String, CountryRepo> countryRepoMap, 
			Map<Integer, List<PreOrderRepo>> preOrderRepoMap, Map<Integer, Status> statusMap) {
		List<BuyDetails> buyDetailsLst = new ArrayList<>();
		for (BuyDetailsRepo buyDetailsRepo : buyDetailsRepoLst) {
			ProductDetailsRepo productDetailsRepo = productDetailsRepoMap.get(buyDetailsRepo.getProductId());
			ProductCategoryRepo productCategoryRepo = productCategoryRepoMap.get(productDetailsRepo.getCategoryId());
			CountryRepo countryRepo = countryRepoMap.get(productDetailsRepo.getCountryCde());
			BuyDetails buyDetails = getBuyDetails(buyDetailsRepo, productDetailsRepo, productCategoryRepo, 
					countryRepo, preOrderRepoMap.get(buyDetailsRepo.getBuyId()), statusMap);
			buyDetailsLst.add(buyDetails);
		}
		return buyDetailsLst;
	}

}
