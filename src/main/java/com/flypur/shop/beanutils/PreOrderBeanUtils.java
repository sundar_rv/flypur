package com.flypur.shop.beanutils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;

import com.flypur.shop.domain.OfferDetails;
import com.flypur.shop.domain.Status;
import com.flypur.shop.repository.model.PreOrderRepo;

public final class PreOrderBeanUtils {
	
	
	public static List<OfferDetails> getPreOrderDetails(List<PreOrderRepo> preOrderRepoLst, Map<Integer, Status> statusMap) {
		List<OfferDetails> offerDetailsLst = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(preOrderRepoLst)) {
			for(PreOrderRepo preOrderRepo : preOrderRepoLst) {
				OfferDetails offerDetails = new OfferDetails();
				offerDetails.setOfferId(preOrderRepo.getTripId());
				offerDetails.setPriceOffered(preOrderRepo.getPriceOffered());
				offerDetails.setStatus(statusMap.get(preOrderRepo.getStatusId()));
				offerDetailsLst.add(offerDetails);
			}
		}
		return offerDetailsLst;
	}

}
