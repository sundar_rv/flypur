package com.flypur.shop.beanutils;

import com.flypur.shop.domain.BuyDetails;
import com.flypur.shop.domain.CollectionMethod;
import com.flypur.shop.domain.ManageOrder;
import com.flypur.shop.domain.OfferDetails;
import com.flypur.shop.domain.Status;
import com.flypur.shop.domain.TripDetails;
import com.flypur.shop.repository.model.ManageOrderRepo;
import com.flypur.shop.repository.model.PreOrderRepo;

public class ManageOrderBeanUtils {
	
	public static PreOrderRepo getPreOrderRepo(ManageOrder manageOrder) {
		PreOrderRepo preOrderRepo = new PreOrderRepo();
		preOrderRepo.setOrderId(manageOrder.getOrderId());
		preOrderRepo.setBuyId(manageOrder.getBuyDetails().getBuyId());
		preOrderRepo.setTripId(manageOrder.getTripDetails().getTripId());
		preOrderRepo.setStatusId(manageOrder.getStatus().getStatusId());
		for(OfferDetails offerDetails : manageOrder.getBuyDetails().getOfferDetails()) {
			if(offerDetails.getOfferId()!=null && offerDetails.getOfferId().equals(manageOrder.getTripDetails().getTripId())) {
				preOrderRepo.setPriceOffered(offerDetails.getPriceOffered());
			}
		}
		return preOrderRepo;
	}

	public static ManageOrderRepo getManageOrderRepo(ManageOrder manageOrder) {
		ManageOrderRepo manageOrderRepo = new ManageOrderRepo();
		manageOrderRepo.setOrderId(manageOrder.getOrderId());
		manageOrderRepo.setLoginId(manageOrder.getLoginId());
		manageOrderRepo.setBuyId(manageOrder.getBuyDetails().getBuyId());
		manageOrderRepo.setTripId(manageOrder.getTripDetails().getTripId());
		manageOrderRepo.setCollectionId(manageOrder.getCollectionMethod().getCollectionId());
		manageOrderRepo.setExpectedDelivery(manageOrder.getExpectedDelivery());
		manageOrderRepo.setStatusId(manageOrder.getStatus().getStatusId());
		manageOrderRepo.setTrackingNo(manageOrder.getTrackingNo());
		return manageOrderRepo;
	}

	public static ManageOrder getManageOrder(ManageOrderRepo manageOrderRepo) {
		ManageOrder manageOrder = new ManageOrder();
		manageOrder.setOrderId(manageOrderRepo.getOrderId());
		manageOrder.setLoginId(manageOrderRepo.getLoginId());
		BuyDetails buyDetails = new BuyDetails();
		buyDetails.setBuyId(manageOrderRepo.getBuyId());
		manageOrder.setBuyDetails(buyDetails);
		
		TripDetails tripDetails = new TripDetails();
		tripDetails.setTripId(manageOrderRepo.getTripId());
		manageOrder.setTripDetails(tripDetails);
		
		CollectionMethod collectionMethod = new CollectionMethod();
		collectionMethod.setCollectionId(manageOrderRepo.getCollectionId());
		manageOrder.setCollectionMethod(collectionMethod);
		
		manageOrder.setExpectedDelivery(manageOrderRepo.getExpectedDelivery());
		
		Status status = new Status();
		status.setStatusId(manageOrderRepo.getStatusId());
		manageOrder.setStatus(status);
		
		manageOrder.setTrackingNo(manageOrderRepo.getTrackingNo());
		return manageOrder;
	}
	
	
	public static ManageOrder getPreOrder(PreOrderRepo preOrderRepo) {
		ManageOrder manageOrder = new ManageOrder();
		manageOrder.setOrderId(preOrderRepo.getOrderId());

		BuyDetails buyDetails = new BuyDetails();
		buyDetails.setBuyId(preOrderRepo.getBuyId());
		manageOrder.setBuyDetails(buyDetails);
		
		TripDetails tripDetails = new TripDetails();
		tripDetails.setTripId(preOrderRepo.getTripId());
		manageOrder.setTripDetails(tripDetails);
		
		Status status = new Status();
		status.setStatusId(preOrderRepo.getStatusId());
		manageOrder.setStatus(status);
		
		return manageOrder;
	}

}
