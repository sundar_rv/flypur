package com.flypur.shop.beanutils;

import com.flypur.shop.domain.TripDetails;
import com.flypur.shop.repository.model.TripDetailsRepo;

public final class TripDetailsBeanUtils {
	
	public static TripDetailsRepo getTripDetailsRepo(TripDetails tripDetails) {
		TripDetailsRepo tripDetailsRepo = null;
		if(tripDetails!=null) {
			tripDetailsRepo = new TripDetailsRepo();
			tripDetailsRepo.setLoginId(tripDetails.getLoginId());
			tripDetailsRepo.setTripId(tripDetails.getTripId());
			tripDetailsRepo.setCountryCde(tripDetails.getCountryCde());
			tripDetailsRepo.setDateRetrn(tripDetails.getDateDlry());
			tripDetailsRepo.setDateDlry(tripDetails.getDateDlry());
			tripDetailsRepo.setSendNotifications(tripDetails.getSendNotifications());
		}
		return tripDetailsRepo;
	}
	
	public static TripDetails getTripDetails(TripDetailsRepo tripDetailsRepo) {
		TripDetails tripDetails = null;
		if(tripDetailsRepo!=null) {
			tripDetails = new TripDetails();
			tripDetails.setLoginId(tripDetailsRepo.getLoginId());
			tripDetails.setTripId(tripDetailsRepo.getTripId());
			tripDetails.setCountryCde(tripDetailsRepo.getCountryCde());
			tripDetails.setDateRetrn(tripDetailsRepo.getDateDlry());
			tripDetails.setDateDlry(tripDetailsRepo.getDateDlry());
			tripDetails.setSendNotifications(tripDetailsRepo.getSendNotifications());
		}
		return tripDetails;
	}

}
