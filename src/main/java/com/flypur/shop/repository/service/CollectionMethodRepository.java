package com.flypur.shop.repository.service;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.flypur.shop.repository.model.CollectionMethodRepo;


public interface CollectionMethodRepository extends PagingAndSortingRepository<CollectionMethodRepo, Integer> {
	
	@Query("SELECT cm FROM CollectionMethodRepo cm WHERE cm.collectionId = :collectionId")
	CollectionMethodRepo findByCollectionId(@Param("collectionId") Integer collectionId);

}
