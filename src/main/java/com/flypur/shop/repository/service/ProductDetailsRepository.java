package com.flypur.shop.repository.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.flypur.shop.repository.model.ProductDetailsRepo;

public interface ProductDetailsRepository extends PagingAndSortingRepository<ProductDetailsRepo, Integer>,
	JpaSpecificationExecutor<ProductDetailsRepo>  {

    @Query("SELECT pd FROM ProductDetailsRepo pd WHERE pd.productId = :productId")
    ProductDetailsRepo findByProductId(@Param("productId") Integer productId);
    
    @Query("SELECT pd FROM ProductDetailsRepo pd WHERE pd.productName like :productName")
    ProductDetailsRepo findByProductName(@Param("productName") String productName);
    
    @Query("SELECT pd FROM ProductDetailsRepo pd WHERE pd.categoryId = :categoryId")
    List<ProductDetailsRepo> findByCategoryId(@Param("categoryId") Integer categoryId);
    
    @Query("SELECT pd FROM ProductDetailsRepo pd WHERE pd.countryCde = :countryCde")
    List<ProductDetailsRepo> findByCountryCde(@Param("countryCde") String countryCde);
    
}
