package com.flypur.shop.repository.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.flypur.shop.repository.model.ManageOrderRepo;

public interface ManageOrderRepository extends PagingAndSortingRepository<ManageOrderRepo, Integer> ,
	JpaSpecificationExecutor<ManageOrderRepo> {
	
	@Query("DELETE ManageOrderRepo mo WHERE mo.buyId = :buyId and mo.statusId = 2")
    void deleteExpireOrders(@Param("buyId") Integer buyId);
	
	@Query("SELECT mo FROM ManageOrderRepo mo WHERE mo.buyId = :buyId")
	List<ManageOrderRepo> getOrderbyBuyId(@Param("buyId") Integer buyId);
	
	@Query("SELECT mo FROM ManageOrderRepo mo WHERE mo.tripId = :tripId")
	List<ManageOrderRepo> getOrderbyTripId(@Param("tripId") Integer tripId);

}
