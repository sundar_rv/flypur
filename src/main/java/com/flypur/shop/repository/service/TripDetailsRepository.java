package com.flypur.shop.repository.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.flypur.shop.repository.model.TripDetailsRepo;

public interface TripDetailsRepository extends PagingAndSortingRepository<TripDetailsRepo, Integer> {

    @Query("SELECT td FROM TripDetailsRepo td WHERE td.loginId = :loginId AND dateRetrn >= SYSDATE()")
    List<TripDetailsRepo> findByLoginId(@Param("loginId") String loginId);
    
    @Query("SELECT td FROM TripDetailsRepo td WHERE td.tripId = :tripId AND td.loginId = :loginId AND dateRetrn >= SYSDATE()")
    TripDetailsRepo findByTripId(@Param("tripId") Integer tripId, @Param("loginId") String loginId);
    
    @Query("SELECT td FROM TripDetailsRepo td WHERE td.countryCde = :countryCde AND td.loginId = :loginId AND dateRetrn >= SYSDATE()")
    List<TripDetailsRepo> findTripByCountry(@Param("countryCde") String countryCde, @Param("loginId") String loginId);
}
