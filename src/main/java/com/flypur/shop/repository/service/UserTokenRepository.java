package com.flypur.shop.repository.service;

import org.springframework.data.repository.CrudRepository;

import com.flypur.shop.repository.model.UserTokenRepo;

public interface UserTokenRepository extends CrudRepository<UserTokenRepo, String> {

}
