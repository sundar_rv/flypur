package com.flypur.shop.repository.service;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.flypur.shop.repository.model.CountryRepo;

public interface CountryRepository extends PagingAndSortingRepository<CountryRepo, String> {

    @Query("SELECT ct FROM CountryRepo ct WHERE ct.countryCde = :countryCde")
    CountryRepo findByCountryCde(@Param("countryCde") String countryCde);
    
}
