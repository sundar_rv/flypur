package com.flypur.shop.repository.service;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.flypur.shop.repository.model.StatusRepo;

public interface StatusRepository extends PagingAndSortingRepository<StatusRepo, Integer> {

    @Query("SELECT st FROM BuyDetailsRepo st WHERE st.statusId = :statusId")
    StatusRepo findByStatusId(@Param("statusId") Integer statusId);
    
}
