package com.flypur.shop.repository.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.flypur.shop.repository.model.ManageOrderRepo;
import com.flypur.shop.repository.model.PreOrderRepo;

public interface PreOrderRepository extends PagingAndSortingRepository<PreOrderRepo, Integer> ,
	JpaSpecificationExecutor<ManageOrderRepo> {
	
	@Query("SELECT mo FROM PreOrderRepo mo WHERE mo.buyId = :buyId")
	List<PreOrderRepo> getOrderbyBuyId(@Param("buyId") Integer buyId);
	
	@Query("SELECT mo FROM PreOrderRepo mo WHERE mo.tripId = :tripId")
	List<PreOrderRepo> getOrderbyTripId(@Param("tripId") Integer tripId);
	
	@Query("DELETE PreOrderRepo mo WHERE mo.buyId = :buyId and mo.statusId = 2")
    void deleteExpireOrders(@Param("buyId") Integer buyId);

}
