package com.flypur.shop.repository.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.flypur.shop.repository.model.BuyDetailsRepo;

public interface BuyDetailsRepository extends PagingAndSortingRepository<BuyDetailsRepo, Integer>,
	JpaSpecificationExecutor<BuyDetailsRepo> {

    @Query("SELECT bd FROM BuyDetailsRepo bd WHERE bd.loginId = :loginId")
    List<BuyDetailsRepo> findByLoginId(@Param("loginId") String loginId);
    
    @Query("SELECT bd FROM BuyDetailsRepo bd WHERE bd.loginId = :loginId AND bd.statusId = :statusId")
    List<BuyDetailsRepo> findByLoginIdStatusId(@Param("loginId") String loginId, @Param("statusId") Integer statusId);
    
    @Query("SELECT bd FROM BuyDetailsRepo bd WHERE bd.loginId = :loginId AND bd.buyId = :buyId")
    BuyDetailsRepo findByLoginIdBuyId(@Param("loginId") String loginId, @Param("buyId") Integer buyId);
    
    @Query("SELECT bd FROM BuyDetailsRepo bd WHERE bd.loginId = :loginId AND bd.productId in "
    		+ "(SELECT pd.productId FROM ProductDetailsRepo pd WHERE pd.countryCde in "
    		+ "(SELECT ct.countryCde FROM CountryRepo ct WHERE ct.countryCde = :countryCde))")
    List<BuyDetailsRepo> findBuyByCountry(@Param("loginId") String loginId, @Param("countryCde") String countryCde);
    
    @Query("SELECT bd FROM BuyDetailsRepo bd WHERE bd.loginId = :loginId AND bd.productId in "
    		+ "(SELECT pd.productId FROM ProductDetailsRepo pd WHERE pd.countryCde in "
    		+ "(SELECT ct.countryCde FROM CountryRepo ct WHERE ct.countryCde = :countryCde))")
    List<BuyDetailsRepo> searchBuyReq(@Param("loginId") String loginId, @Param("countryCde") String countryCde);
}
