package com.flypur.shop.repository.service;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.flypur.shop.repository.model.UserRepo;

public interface UserRepository extends PagingAndSortingRepository<UserRepo, String> {

    @Query("SELECT u FROM UserRepo u WHERE u.email = :email")
    UserRepo findByEmail(@Param("email") String email);
    
    @Query("SELECT u FROM UserRepo u WHERE u.loginId = :loginId")
    UserRepo findByLoginId(@Param("loginId") String loginId);
}
