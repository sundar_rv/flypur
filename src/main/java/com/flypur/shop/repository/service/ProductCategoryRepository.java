package com.flypur.shop.repository.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.flypur.shop.repository.model.ProductCategoryRepo;

public interface ProductCategoryRepository extends PagingAndSortingRepository<ProductCategoryRepo, Integer>,
	JpaSpecificationExecutor<ProductCategoryRepo> {
  
    @Query("SELECT pc FROM ProductCategoryRepo pc WHERE pc.categoryId = :categoryId")
    ProductCategoryRepo findByCategoryId(@Param("categoryId") Integer categoryId);
    
    @Query("SELECT pc FROM ProductCategoryRepo pc WHERE pc.categoryName = :categoryName")
    List<ProductCategoryRepo> findByCategoryName(@Param("categoryName") String categoryName);
    
}
