package com.flypur.shop.repository.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.flypur.shop.repository.model.UserAddressRepo;

public interface UserAddressRepository extends PagingAndSortingRepository<UserAddressRepo, String> {

    @Query("SELECT u FROM UserAddressRepo u WHERE u.city = :city")
    List<UserAddressRepo> findByCity(@Param("city") String city);
    
    @Query("SELECT u FROM UserAddressRepo u WHERE u.loginId = :loginId")
    List<UserAddressRepo> findByLoginId(@Param("loginId") String loginId);
}
