package com.flypur.shop.repository.spec;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import com.flypur.shop.domain.KeyValue;
import com.flypur.shop.domain.SearchParam;
import com.flypur.shop.repository.model.BuyDetailsRepo;
import com.flypur.shop.repository.model.BuyDetailsRepo_;
import com.flypur.shop.repository.model.ManageOrderRepo;
import com.flypur.shop.repository.model.ManageOrderRepo_;
import com.flypur.shop.repository.model.ProductDetailsRepo;
import com.flypur.shop.repository.model.ProductDetailsRepo_;
import com.flypur.shop.util.Constants;
import com.flypur.shop.util.DateUtil;

public class SearchSpec {
	
	public static Specification<BuyDetailsRepo> buySearch(final SearchParam searchParam, String loginId) {
		return new Specification<BuyDetailsRepo>() {
			@Override
			public Predicate toPredicate(Root<BuyDetailsRepo> buyDetails, CriteriaQuery<?> query, CriteriaBuilder builder) {
				if (CollectionUtils.isEmpty(searchParam.getKeyValue())) {
					throw new IllegalStateException("At least one parameter should be provided to construct complex query");
				}
				List<Predicate> predicates = new ArrayList<Predicate>();
				if(loginId!=null) {
					predicates.add(builder.and(builder.equal(buyDetails.get(BuyDetailsRepo_.loginId), loginId)));
				}
				for (KeyValue keyVal : searchParam.getKeyValue()) {
					 if (Constants.BUY_SEARCH.BUYID.getValue().equals(keyVal.getKey())) {
							predicates.add(builder.and(builder.equal(buyDetails.get(BuyDetailsRepo_.buyId), Integer.parseInt(keyVal.getValue()))));
					} else if(Constants.BUY_SEARCH.EXPDATE.getValue().equals(keyVal.getKey())) {
						if(Constants.SEARCH_OPERTR.EQ.getValue().equals(keyVal.getOperation())) {
							predicates.add(builder.and(builder.equal(buyDetails.get(BuyDetailsRepo_.reqExpireDate), DateUtil.strToLocalDate(keyVal.getValue()))));
						} else if (Constants.SEARCH_OPERTR.GT.getValue().equals(keyVal.getOperation())) {
							predicates.add(builder.and(builder.greaterThan(buyDetails.get(BuyDetailsRepo_.reqExpireDate), DateUtil.strToLocalDate(keyVal.getValue()))));
						} else if (Constants.SEARCH_OPERTR.GTE.getValue().equals(keyVal.getOperation())) {
							predicates.add(builder.and(builder.greaterThanOrEqualTo(buyDetails.get(BuyDetailsRepo_.reqExpireDate), DateUtil.strToLocalDate(keyVal.getValue()))));
						} else if (Constants.SEARCH_OPERTR.LT.getValue().equals(keyVal.getOperation())) {
							predicates.add(builder.and(builder.lessThan(buyDetails.get(BuyDetailsRepo_.reqExpireDate), DateUtil.strToLocalDate(keyVal.getValue()))));
						} else if (Constants.SEARCH_OPERTR.LTE.getValue().equals(keyVal.getOperation())) {
							predicates.add(builder.and(builder.lessThanOrEqualTo(buyDetails.get(BuyDetailsRepo_.reqExpireDate), DateUtil.strToLocalDate(keyVal.getValue()))));
						}
					} else if(Constants.BUY_SEARCH.ODRDATE.getValue().equals(keyVal.getKey())) {
						if(Constants.SEARCH_OPERTR.EQ.getValue().equals(keyVal.getOperation())) {
							predicates.add(builder.and(builder.equal(buyDetails.get(BuyDetailsRepo_.orderDate), DateUtil.strToLocalDate(keyVal.getValue()))));
						} else if (Constants.SEARCH_OPERTR.GT.getValue().equals(keyVal.getOperation())) {
							predicates.add(builder.and(builder.greaterThan(buyDetails.get(BuyDetailsRepo_.orderDate), DateUtil.strToLocalDate(keyVal.getValue()))));
						} else if (Constants.SEARCH_OPERTR.GTE.getValue().equals(keyVal.getOperation())) {
							predicates.add(builder.and(builder.greaterThanOrEqualTo(buyDetails.get(BuyDetailsRepo_.orderDate), DateUtil.strToLocalDate(keyVal.getValue()))));
						} else if (Constants.SEARCH_OPERTR.LT.getValue().equals(keyVal.getOperation())) {
							predicates.add(builder.and(builder.lessThan(buyDetails.get(BuyDetailsRepo_.orderDate), DateUtil.strToLocalDate(keyVal.getValue()))));
						} else if (Constants.SEARCH_OPERTR.LTE.getValue().equals(keyVal.getOperation())) {
							predicates.add(builder.and(builder.lessThanOrEqualTo(buyDetails.get(BuyDetailsRepo_.orderDate), DateUtil.strToLocalDate(keyVal.getValue()))));
						}
					} else if (Constants.BUY_SEARCH.STATUS.getValue().equals(keyVal.getKey())) {
						predicates.add(builder.and(builder.equal(buyDetails.get(BuyDetailsRepo_.statusId), keyVal.getValue())));
					}
				}
				Predicate[] predicatesArray = new Predicate[predicates.size()];
				return builder.and(predicates.toArray(predicatesArray));
			}
		};
	}

	public static Specification<ProductDetailsRepo> productSearch(SearchParam searchParam) {
		return new Specification<ProductDetailsRepo>() {
			@Override
			public Predicate toPredicate(Root<ProductDetailsRepo> productDetails, CriteriaQuery<?> query, CriteriaBuilder builder) {
				if (CollectionUtils.isEmpty(searchParam.getKeyValue())) {
					throw new IllegalStateException("At least one parameter should be provided to construct complex query");
				}
				List<Predicate> predicates = new ArrayList<Predicate>();
				for (KeyValue keyVal : searchParam.getKeyValue()) {
					if(Constants.PRODUCT_SEARCH.NAME.getValue().equals(keyVal.getKey())) {
						predicates.add(builder.and(builder.like(productDetails.get(ProductDetailsRepo_.productName), getContainsLikePattern(keyVal.getValue()))));
					} else if(Constants.PRODUCT_SEARCH.CTGRY.getValue().equals(keyVal.getKey())) {
						predicates.add(builder.and(builder.equal(productDetails.get(ProductDetailsRepo_.categoryId), Integer.parseInt(keyVal.getValue()))));
					} else if(Constants.PRODUCT_SEARCH.SUBCTGRY.getValue().equals(keyVal.getKey())) {
						predicates.add(builder.and(builder.equal(productDetails.get(ProductDetailsRepo_.categoryId), Integer.parseInt(keyVal.getValue()))));
					} else if (Constants.PRODUCT_SEARCH.COUNTRY.getValue().equals(keyVal.getKey())) {
						predicates.add(builder.and(builder.equal(productDetails.get(ProductDetailsRepo_.countryCde), keyVal.getValue())));
					}
				}
				Predicate[] predicatesArray = new Predicate[predicates.size()];
				return builder.and(predicates.toArray(predicatesArray));
			}
		};
	}
	
	public static Specification<ManageOrderRepo> orderSearch(SearchParam searchParam) {
		return new Specification<ManageOrderRepo>() {
			@Override
			public Predicate toPredicate(Root<ManageOrderRepo> manageOrder, CriteriaQuery<?> query, CriteriaBuilder builder) {
				if (CollectionUtils.isEmpty(searchParam.getKeyValue())) {
					throw new IllegalStateException("At least one parameter should be provided to construct complex query");
				}
				List<Predicate> predicates = new ArrayList<Predicate>();
				for (KeyValue keyVal : searchParam.getKeyValue()) {
					if(Constants.ORDER_SEARCH.LOGINID.getValue().equals(keyVal.getKey())) {
						predicates.add(builder.and(builder.like(manageOrder.get(ManageOrderRepo_.loginId), getContainsLikePattern(keyVal.getValue()))));
					} else if(Constants.ORDER_SEARCH.BUYID.getValue().equals(keyVal.getKey())) {
						predicates.add(builder.and(builder.equal(manageOrder.get(ManageOrderRepo_.buyId), Integer.parseInt(keyVal.getValue()))));
					} else if(Constants.ORDER_SEARCH.TRIPID.getValue().equals(keyVal.getKey())) {
						predicates.add(builder.and(builder.equal(manageOrder.get(ManageOrderRepo_.tripId), Integer.parseInt(keyVal.getValue()))));
					} else if (Constants.ORDER_SEARCH.STATUSID.getValue().equals(keyVal.getKey())) {
						predicates.add(builder.and(builder.equal(manageOrder.get(ManageOrderRepo_.statusId), keyVal.getValue())));
					}
				}
				Predicate[] predicatesArray = new Predicate[predicates.size()];
				return builder.and(predicates.toArray(predicatesArray));
			}
		};
	}
	
	private static String getContainsLikePattern(String searchTerm) {
        return "%" + searchTerm.toLowerCase() + "%";
    }
}
