package com.flypur.shop.repository.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CountryRepo.class)
public abstract class CountryRepo_ {

	public static volatile SingularAttribute<CountryRepo, String> countryCde;
	public static volatile SingularAttribute<CountryRepo, String> countryName;

}

