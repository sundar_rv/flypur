package com.flypur.shop.repository.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PreOrderRepo.class)
public abstract class PreOrderRepo_ {

	public static volatile SingularAttribute<PreOrderRepo, Integer> statusId;
	public static volatile SingularAttribute<PreOrderRepo, Integer> orderId;
	public static volatile SingularAttribute<PreOrderRepo, Integer> buyId;
	public static volatile SingularAttribute<PreOrderRepo, Integer> tripId;

}

