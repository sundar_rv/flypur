package com.flypur.shop.repository.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "product_details")
@XmlRootElement
public class ProductDetailsRepo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "product_id")
    private Integer productId;
    
    @Basic(optional = false)
    @Column(name = "product_name")
    private String productName;
    
    @Basic(optional = false)
    @Column(name = "category_id")
    private Integer categoryId;
    
    @Basic(optional = false)
    @Column(name = "picture")
    private Integer picture;
    
    @Basic(optional = false)
    @Column(name = "product_url")
    private String productUrl;
    
    @Basic(optional = false)
    @Column(name = "country_cde")
    private String countryCde;
    
    @Basic(optional = false)
    @Column(name = "product_type")
    private String productType;
    
    @Basic(optional = false)
    @Column(name = "product_size")
    private String productSize;

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public Integer getPicture() {
		return picture;
	}

	public void setPicture(Integer picture) {
		this.picture = picture;
	}

	public String getProductUrl() {
		return productUrl;
	}

	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}

	public String getCountryCde() {
		return countryCde;
	}

	public void setCountryCde(String countryCde) {
		this.countryCde = countryCde;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getProductSize() {
		return productSize;
	}

	public void setProductSize(String productSize) {
		this.productSize = productSize;
	}

}
