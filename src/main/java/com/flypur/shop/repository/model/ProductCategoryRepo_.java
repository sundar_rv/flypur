package com.flypur.shop.repository.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProductCategoryRepo.class)
public abstract class ProductCategoryRepo_ {

	public static volatile SingularAttribute<ProductCategoryRepo, String> categoryName;
	public static volatile SingularAttribute<ProductCategoryRepo, Integer> categoryId;
	public static volatile SingularAttribute<ProductCategoryRepo, String> subCategoryName;

}

