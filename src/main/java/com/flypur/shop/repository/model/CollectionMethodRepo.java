package com.flypur.shop.repository.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "collection_method")
@XmlRootElement
public class CollectionMethodRepo implements Serializable {
	
	 private static final long serialVersionUID = 1L;
	    
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Basic(optional = false)
	    @Column(name = "collection_id")
	    private Integer collectionId;
	    
	    @Basic(optional = false)
	    @Column(name = "method_name")
	    private String methodName;

		public Integer getCollectionId() {
			return collectionId;
		}

		public void setCollectionId(Integer collectionId) {
			this.collectionId = collectionId;
		}

		public String getMethodName() {
			return methodName;
		}

		public void setMethodName(String methodName) {
			this.methodName = methodName;
		}

}
