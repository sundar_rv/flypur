package com.flypur.shop.repository.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "preorder")
@XmlRootElement
public class PreOrderRepo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "order_id")
    private Integer orderId;
    
    @Basic(optional = false)
    @Column(name = "buy_id")
    private Integer buyId;
    
    @Basic(optional = false)
    @Column(name = "trip_id")
    private Integer tripId;
    
    @Basic(optional = false)
    @Column(name = "price_offered")
    private BigDecimal priceOffered;
    
    @Basic(optional = false)
    @Column(name = "status_id")
    private Integer statusId;
    
    public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getBuyId() {
		return buyId;
	}

	public void setBuyId(Integer buyId) {
		this.buyId = buyId;
	}

	public Integer getTripId() {
		return tripId;
	}

	public void setTripId(Integer tripId) {
		this.tripId = tripId;
	}

	public BigDecimal getPriceOffered() {
		return priceOffered;
	}

	public void setPriceOffered(BigDecimal priceOffered) {
		this.priceOffered = priceOffered;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
}
