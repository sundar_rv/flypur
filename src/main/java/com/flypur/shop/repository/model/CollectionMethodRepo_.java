package com.flypur.shop.repository.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CollectionMethodRepo.class)
public abstract class CollectionMethodRepo_ {

	public static volatile SingularAttribute<CollectionMethodRepo, String> methodName;
	public static volatile SingularAttribute<CollectionMethodRepo, Integer> collectionId;

}

