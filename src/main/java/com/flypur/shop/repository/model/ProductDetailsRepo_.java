package com.flypur.shop.repository.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProductDetailsRepo.class)
public abstract class ProductDetailsRepo_ {

	public static volatile SingularAttribute<ProductDetailsRepo, Integer> productId;
	public static volatile SingularAttribute<ProductDetailsRepo, String> countryCde;
	public static volatile SingularAttribute<ProductDetailsRepo, String> productSize;
	public static volatile SingularAttribute<ProductDetailsRepo, String> productUrl;
	public static volatile SingularAttribute<ProductDetailsRepo, String> productName;
	public static volatile SingularAttribute<ProductDetailsRepo, Integer> categoryId;
	public static volatile SingularAttribute<ProductDetailsRepo, Integer> picture;
	public static volatile SingularAttribute<ProductDetailsRepo, String> productType;

}

