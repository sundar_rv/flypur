package com.flypur.shop.repository.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.joda.time.LocalDate;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TripDetailsRepo.class)
public abstract class TripDetailsRepo_ {

	public static volatile SingularAttribute<TripDetailsRepo, String> loginId;
	public static volatile SingularAttribute<TripDetailsRepo, LocalDate> dateRetrn;
	public static volatile SingularAttribute<TripDetailsRepo, String> countryCde;
	public static volatile SingularAttribute<TripDetailsRepo, Integer> tripId;
	public static volatile SingularAttribute<TripDetailsRepo, String> sendNotifications;
	public static volatile SingularAttribute<TripDetailsRepo, LocalDate> dateDlry;
	public static volatile SingularAttribute<TripDetailsRepo, Integer> statusId;
}

