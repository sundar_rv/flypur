package com.flypur.shop.repository.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "user_addresses")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserAddress.findAll", query = "SELECT a FROM UserAddressRepo a"),
    @NamedQuery(name = "UserAddress.findByAddressId", query = "SELECT a FROM UserAddressRepo a WHERE a.addressId = :addressId"),
    @NamedQuery(name = "UserAddress.findByLoginId", query = "SELECT a FROM UserAddressRepo a WHERE a.loginId = :loginId"),
    @NamedQuery(name = "UserAddress.findByCity", query = "SELECT a FROM UserAddressRepo a WHERE a.city = :city"),
    @NamedQuery(name = "UserAddress.findByMobile", query = "SELECT a FROM UserAddressRepo a WHERE a.mobile = :mobile"),
    @NamedQuery(name = "UserAddress.findByPhone", query = "SELECT a FROM UserAddressRepo a WHERE a.phone = :phone"),
    @NamedQuery(name = "UserAddress.findByCountry", query = "SELECT a FROM UserAddressRepo a WHERE a.country = :country")})
public class UserAddressRepo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "address_id")
    private Integer addressId;
    
    @Basic(optional = false)
    @Column(name = "login_id")
    private String loginId;
    
    @Basic(optional = false)
    @Column(name = "address_line1")
    private String addressLine1;
    
    @Basic(optional = false)
    @Column(name = "address_line2")
    private String addressLine2;
    
    @Basic(optional = false)
    @Column(name = "phone")
    private String phone;
    
    @Basic(optional = false)
    @Column(name = "mobile")
    private String mobile;
    
    @Basic(optional = false)
    @Column(name = "city")
    private String city;
    
    @Basic(optional = false)
    @Column(name = "pincode")
    private String pincode;
    
    @Basic(optional = false)
    @Column(name = "country")
    private String country;

    public UserAddressRepo() {
    }

    public UserAddressRepo(Integer addressId) {
        this.addressId = addressId;
    }

    public UserAddressRepo(Integer adressId, String loginId, String addressLine1, String addressLine2, String phone, String mobile, String city, String pincode, String country) {
        this.addressId = adressId;
        this.loginId = loginId;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.phone = phone;
        this.mobile = mobile;
        this.city = city;
        this.pincode = pincode;
        this.country = country;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (addressId != null ? addressId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof UserAddressRepo)) {
            return false;
        }
        UserAddressRepo other = (UserAddressRepo) object;
        if ((this.addressId == null && other.addressId != null) || (this.addressId != null && !this.addressId.equals(other.addressId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flypur.shop.web.repository.model[ addressId=" + addressId + " ]";
    }

}
