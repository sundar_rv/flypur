package com.flypur.shop.repository.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.joda.time.LocalDate;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ManageOrderRepo.class)
public abstract class ManageOrderRepo_ {

	public static volatile SingularAttribute<ManageOrderRepo, String> loginId;
	public static volatile SingularAttribute<ManageOrderRepo, LocalDate> expectedDelivery;
	public static volatile SingularAttribute<ManageOrderRepo, Integer> statusId;
	public static volatile SingularAttribute<ManageOrderRepo, Integer> orderId;
	public static volatile SingularAttribute<ManageOrderRepo, String> trackingNo;
	public static volatile SingularAttribute<ManageOrderRepo, Integer> buyId;
	public static volatile SingularAttribute<ManageOrderRepo, Integer> tripId;
	public static volatile SingularAttribute<ManageOrderRepo, Integer> collectionId;

}

