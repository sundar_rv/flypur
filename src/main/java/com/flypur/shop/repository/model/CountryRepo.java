package com.flypur.shop.repository.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "country")
@XmlRootElement
public class CountryRepo implements Serializable {
	
	 private static final long serialVersionUID = 1L;
	    
	    @Id
	    @Basic(optional = false)
	    @Column(name = "country_cde")
	    private String countryCde;
	    
	    @Basic(optional = false)
	    @Column(name = "country_name")
	    private String countryName;

		public String getCountryCde() {
			return countryCde;
		}

		public void setCountryCde(String countryCde) {
			this.countryCde = countryCde;
		}

		public String getCountryName() {
			return countryName;
		}

		public void setCountryName(String countryName) {
			this.countryName = countryName;
		}


}
