package com.flypur.shop.repository.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserTokenRepo.class)
public abstract class UserTokenRepo_ {

	public static volatile SingularAttribute<UserTokenRepo, String> loginId;
	public static volatile SingularAttribute<UserTokenRepo, Date> loginDate;
	public static volatile SingularAttribute<UserTokenRepo, String> token;
	public static volatile SingularAttribute<UserTokenRepo, Date> expirationDate;

}

