package com.flypur.shop.repository.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.flypur.shop.util.LocalDateSerializer;

@Entity
@Table(name = "trip_details")
@XmlRootElement
public class TripDetailsRepo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "trip_id")
    private Integer tripId;
    
    @Basic(optional = false)
    @Column(name = "login_id")
    private String loginId;
    
    @Basic(optional = false)
    @Column(name = "country_cde")
    private String countryCde;
    
    @Basic(optional = false)
    @Column(name = "date_retrn")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateRetrn;
    
    @Basic(optional = false)
    @Column(name = "date_dlry")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateDlry;
    
    @Basic(optional = false)
    @Column(name = "status_id")
    private Integer statusId;

    @Basic(optional = true)
    @Column(name = "send_notifications")
    private String sendNotifications;
    
    public TripDetailsRepo() {
    }

    public TripDetailsRepo(Integer tripId, String loginId, String countryCde, LocalDate dateRetrn, LocalDate dateDlry, String sendNotifications) {
    	this.tripId = tripId;
        this.loginId = loginId;
        this.countryCde = countryCde;
        this.dateRetrn = dateRetrn;
        this.dateDlry = dateDlry;
        this.sendNotifications = sendNotifications;
    }

	public Integer getTripId() {
		return tripId;
	}

	public void setTripId(Integer tripId) {
		this.tripId = tripId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getCountryCde() {
		return countryCde;
	}

	public void setCountryCde(String countryCde) {
		this.countryCde = countryCde;
	}

	public LocalDate getDateRetrn() {
		return dateRetrn;
	}

	public void setDateRetrn(LocalDate dateRetrn) {
		this.dateRetrn = dateRetrn;
	}

	public LocalDate getDateDlry() {
		return dateDlry;
	}

	public void setDateDlry(LocalDate dateDlry) {
		this.dateDlry = dateDlry;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public String getSendNotifications() {
		return sendNotifications;
	}

	public void setSendNotifications(String sendNotifications) {
		this.sendNotifications = sendNotifications;
	}
}
