package com.flypur.shop.repository.model;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import org.joda.time.LocalDate;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BuyDetailsRepo.class)
public abstract class BuyDetailsRepo_ {

	public static volatile SingularAttribute<BuyDetailsRepo, LocalDate> reqExpireDate;
	public static volatile SingularAttribute<BuyDetailsRepo, String> loginId;
	public static volatile SingularAttribute<BuyDetailsRepo, Integer> quantity;
	public static volatile SingularAttribute<BuyDetailsRepo, Integer> productId;
	public static volatile SingularAttribute<BuyDetailsRepo, Integer> statusId;
	public static volatile SingularAttribute<BuyDetailsRepo, BigDecimal> priceOffered;
	public static volatile SingularAttribute<BuyDetailsRepo, Integer> buyId;
	public static volatile SingularAttribute<BuyDetailsRepo, String> sendNotifications;
	public static volatile SingularAttribute<BuyDetailsRepo, LocalDate> orderDate;

}

