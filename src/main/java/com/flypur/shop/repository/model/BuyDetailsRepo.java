package com.flypur.shop.repository.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.flypur.shop.util.LocalDateSerializer;

@Entity
@Table(name = "buy_details")
@XmlRootElement
public class BuyDetailsRepo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "buy_id")
    private Integer buyId;
    
    @Basic(optional = false)
    @Column(name = "login_id")
    private String loginId;
    
    @Basic(optional = false)
    @Column(name = "product_id")
    private Integer productId;
    
    @Basic(optional = false)
    @Column(name = "quantity")
    private Integer quantity;
    
    @Basic(optional = false)
    @Column(name = "req_expire_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate reqExpireDate;
    
    @Basic(optional = false)
    @Column(name = "status_id")
    private Integer statusId;
    
    @Basic(optional = true)
    @Column(name = "send_notifications")
    private String sendNotifications;
    
    @Basic(optional = false)
    @Column(name = "order_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate orderDate;

	public Integer getBuyId() {
		return buyId;
	}

	public void setBuyId(Integer buyId) {
		this.buyId = buyId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public LocalDate getReqExpireDate() {
		return reqExpireDate;
	}

	public void setReqExpireDate(LocalDate reqExpireDate) {
		this.reqExpireDate = reqExpireDate;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public String getSendNotifications() {
		return sendNotifications;
	}

	public void setSendNotifications(String sendNotifications) {
		this.sendNotifications = sendNotifications;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}
	
}
