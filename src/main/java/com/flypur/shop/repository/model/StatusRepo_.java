package com.flypur.shop.repository.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(StatusRepo.class)
public abstract class StatusRepo_ {

	public static volatile SingularAttribute<StatusRepo, Integer> statusId;
	public static volatile SingularAttribute<StatusRepo, String> statusName;

}

