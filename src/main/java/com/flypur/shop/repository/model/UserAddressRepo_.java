package com.flypur.shop.repository.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserAddressRepo.class)
public abstract class UserAddressRepo_ {

	public static volatile SingularAttribute<UserAddressRepo, String> pincode;
	public static volatile SingularAttribute<UserAddressRepo, String> country;
	public static volatile SingularAttribute<UserAddressRepo, String> loginId;
	public static volatile SingularAttribute<UserAddressRepo, String> phone;
	public static volatile SingularAttribute<UserAddressRepo, String> city;
	public static volatile SingularAttribute<UserAddressRepo, String> mobile;
	public static volatile SingularAttribute<UserAddressRepo, String> addressLine1;
	public static volatile SingularAttribute<UserAddressRepo, String> addressLine2;
	public static volatile SingularAttribute<UserAddressRepo, Integer> addressId;

}

