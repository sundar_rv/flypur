package com.flypur.shop.repository.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserRepo.class)
public abstract class UserRepo_ {

	public static volatile SingularAttribute<UserRepo, String> firstName;
	public static volatile SingularAttribute<UserRepo, String> lastName;
	public static volatile SingularAttribute<UserRepo, String> loginId;
	public static volatile SingularAttribute<UserRepo, Date> updateDatetime;
	public static volatile SingularAttribute<UserRepo, String> salt;
	public static volatile SingularAttribute<UserRepo, Integer> roleId;
	public static volatile SingularAttribute<UserRepo, String> middleName;
	public static volatile SingularAttribute<UserRepo, String> email;
	public static volatile SingularAttribute<UserRepo, String> passwordHash;
	public static volatile SingularAttribute<UserRepo, Integer> status;
	public static volatile SingularAttribute<UserRepo, Date> createDate;

}

