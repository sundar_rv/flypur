package com.flypur.shop.util;

public class Constants {
	
	// API FOMAT DATE
    public static final String API_FORMAT_DATE = "yyyy/MM/dd HH:mm:ss";
    
    public static final long ONEDAY_MILLISEC = 86400000; // 1 day
    public static final long THIRTYMIN_MILLISEC = 1800000; // 30 minutes
    
    public static enum STATUS {
        ACTIVE_STATUS(0, "Active"),
        DELETED_STATUS(1, "Deleted"),
        REVOKE_STATUS(2, "Revoke"),
        DISABLED_STATUS(3, "Disable"),
        DELETED_FOREVER_STATUS(4, "Deleted forever"),
        PENDING(5, "Pending"),
        TRIAL_ACCOUNT_STATUS(6, "Trial");

        private final int value;
        private final String type;

        private STATUS(int value, String type) {
            this.value = value;
            this.type = type;
        }

        public int getValue() {
            return value;
        }

        public String getType() {
            return type;
        }
    }

    public static enum MESSAGE_STATUS {
        UNREAD(0, "unread"),
        READED(1, "readed");

        private final int value;
        private final String type;

        private MESSAGE_STATUS(int value, String type) {
            this.value = value;
            this.type = type;
        }

        public int getValue() {
            return value;
        }

        public String getType() {
            return type;
        }
    }

    public static enum USER_ROLE {
        ANONYMOUS_USER(1, "Anonymous User"),
        REGISTED_USER(2, "Registed User");

        private final int roleId;
        private final String roleName;

        private USER_ROLE(int id, String name) {
            this.roleId = id;
            this.roleName = name;
        }

        public int getRoleId() {
            return roleId;
        }

        public String getRoleName() {
            return roleName;
        }
    }
    
    public static enum USER_STATUS {
        INACTIVE(-1),
        PENDING(0),
        ACTIVE(1);
        
        private final int status;
        
        private USER_STATUS(int status) {
            this.status = status;
        }
        
        public int getStatus() {
            return status;
        }
    }
    
    public static enum ORDER_STATUS {
        PENDING(0),
        SHIPPING(1),
        COMPLETED(2);
        
        private final int status;
        
        private ORDER_STATUS(int status) {
            this.status = status;
        }
        
        public int getStatus() {
            return status;
        }
    }
    
    public static enum TRIP_SEARCH {
        LOGINID("LOGINID"),
        COUNTRY("COUNTRY");
        
        private final String value;
        
        private TRIP_SEARCH(String value) {
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }
    }
    
    public static enum BUY_SEARCH {
    	BUYID("BUYID"),
        LOGINID("LOGINID"),
        COUNTRY("COUNTRY"),
    	STATUS("STATUS"),
    	EXPDATE("EXPDATE"),
    	ODRDATE("ODRDATE"),
    	CATGRY("CATGRY");
        
        private final String value;
        
        private BUY_SEARCH(String value) {
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }
    }
    
    public static enum PRODUCT_SEARCH {
        NAME("NAME"),
        CTGRY("CTGRY"),
    	SUBCTGRY("SUBCTGRY"),
    	COUNTRY("COUNTRY");
        
        private final String value;
        
        private PRODUCT_SEARCH(String value) {
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }
    }
    
    public static enum ORDER_SEARCH {
    	LOGINID("LOGINID"),
        BUYID("BUYID"),
    	TRIPID("TRIPID"),
    	STATUSID("STATUSID");
        
        private final String value;
        
        private ORDER_SEARCH(String value) {
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }
    }
    
    public static enum SEARCH_OPERTR {
    	
    	GT(">"),
        LT("<"),
    	GTE(">="),
    	LTE("<="),
    	EQ("="),
    	LIKE("%");
        
        private final String value;
        
        private SEARCH_OPERTR(String value) {
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }
    }
}
