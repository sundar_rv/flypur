package com.flypur.shop.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.flypur.shop.api.response.StatusResponse;
import com.flypur.shop.config.AppConfig;
import com.flypur.shop.exception.ApplicationException;
import com.flypur.shop.mail.EmailService;
import com.flypur.shop.service.UserService;
import com.flypur.shop.service.UserTokenService;

import java.text.SimpleDateFormat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Setting up some stuff using for all API
 *
 */
public abstract class APIUtil {

	@Autowired
	protected AppConfig appConfig;
	
	@Autowired
	protected UserService userService;
    
    @Autowired
    protected UserTokenService userTokenService;
    
    @Autowired
    protected EmailService emailService;

	public StatusResponse<?> statusResponse = null;
	public final static ObjectMapper mapper = new ObjectMapper();

	static {
		mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
				.setSerializationInclusion(JsonInclude.Include.ALWAYS)
				.setDateFormat(new SimpleDateFormat(Constants.API_FORMAT_DATE));
	}

	public final static Logger logger = LogManager.getLogger(APIUtil.class.getName());

	protected String writeObjectToJson(Object obj) {
		try {
			return mapper.writeValueAsString(obj);
		} catch (JsonProcessingException ex) {
			throw new ApplicationException(ex.getCause());
		}
	}

	protected String writeObjectToJsonRemoveNullProperty(Object obj) throws ApplicationException {
		try {
			mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
			String result = mapper.writeValueAsString(obj);
			mapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
			return result;
		} catch (JsonProcessingException ex) {
			throw new ApplicationException(ex.getCause());
		}
	}
}
