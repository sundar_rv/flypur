package com.flypur.shop.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;

public class DateUtil {
	
	/**
     * Convert date to UTC standard based on local TimeZone
     * @param date to be converted
     * @return converted date
     */
    public static Date convertToUTC(Date date){
        DateTimeZone d = DateTimeZone.getDefault();
        return new Date(d.convertLocalToUTC(date.getTime(), false));
    }
    
    public static Date strToDate(String dateStr) {
    	Date date = null;
    	if(dateStr != null) {
    		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    		try {
    			date = df.parse(dateStr);
    		} catch (ParseException e) {
    		    e.printStackTrace();
    		}
    	}
    	return date;
    }
    
    public static LocalDate strToLocalDate(String dateStr) {
    	Date date = null;
    	if(dateStr != null) {
    		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    		try {
    			date = df.parse(dateStr);
    		} catch (ParseException e) {
    		    e.printStackTrace();
    		}
    	}
    	return new LocalDate(date);
    }

}
