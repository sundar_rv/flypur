/*
 * 
 */
package com.flypur.shop.util;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

/**
 * 128 bit UUID (Universal Unique ID) 
 *
 */

public class UniqueID
{
    /**
     * 128 bit UUID 
     */
    public static synchronized String getUUID() {
	String uuid = UUID.randomUUID().toString();
	return StringUtils.replace(uuid, "-", "");
    }
}
