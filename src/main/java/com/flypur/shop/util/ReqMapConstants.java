package com.flypur.shop.util;

public class ReqMapConstants {
	
	// version
    public static final String FLYPUR = "flypur";
	
	// version
    public static final String VERSION = "/api/v1";
    
    // charset
    public static final String CHARSET = "application/json;charset=utf-8";
	
	//Users API
    public static final String USERS = FLYPUR+VERSION + "/users";
	//public static final String USERS = VERSION + "/users";//Use this to view Swagger API
	public static final String USERS_REGISTER = "/register";
	public static final String USERS_ADD_ADDRESS = "/address/add";
	public static final String USERS_ACTIVATE_USER = "/verifyEmail/{id}";
	public static final String USERS_LOGIN = "/login";
    public static final String USERS_LOGOUT = "/logout";
    
    //TripReq API
    public static final String TRIP = FLYPUR+VERSION + "/trip";
    //public static final String TRIP = VERSION + "/trip";//Use this to view Swagger API
    public static final String TRIP_GET = "/getTrip";
    public static final String TRIP_GET_SEARCH = "/getTrips";
    public static final String TRIP_ADD = "/addTrip";
    public static final String TRIP_UPDATE = "/updateTrip";
    public static final String TRIP_DELETE = "/deleteTrip";
    public static final String PRODUCT_SEARCH = "/searchProduct";
    
    //BuyReq API
    public static final String BUY = FLYPUR+VERSION + "/buy";
    //public static final String BUY = VERSION + "/buy";//Use this to view Swagger API
    public static final String BUY_GET = "/getBuyReq";
    public static final String BUY_GET_SEARCH = "/getBuyReqs";
    public static final String BUY_ADD = "/addBuyReq";
    public static final String BUY_UPDATE = "/updateBuyReq";
    public static final String BUY_DELETE = "/deleteBuyReq";
    
    //ManagerOrder API
    public static final String MANAGE = FLYPUR+VERSION + "/manage";
    //public static final String MANAGE = VERSION + "/manage";//Use this to view Swagger API
    public static final String PREORDER_ADD = "/addPreOrder";
    public static final String PREORDER_UPDATE_STATUS = "/updatePreOrderStatus";
    public static final String ORDER_UPDATE = "/updateOrder";
    public static final String ORDER_UPDATE_STATUS = "/updateOrderStatus";
    public static final String GET_ORDER = "/getOrder";
    public static final String GET_ORDERS = "/getOrders";
    
    
    //Utils API
    public static final String UTILS = FLYPUR+VERSION + "/utils";
    //public static final String BUY = VERSION + "/buy";//Use this to view Swagger API
    public static final String UTIL_GET_ALL_CNTRYLIST = "/getAllCountries";
    public static final String UTIL_GET_CNTRY_NAME = "/getCountryName/{country_cde}";
    public static final String UTIL_GET_ALL_STATUSLIST = "/getAllStatus";
    public static final String UTIL_GET_STATUS_NAME = "/getStatusName/{id}";
    public static final String UTIL_GET_ALL_CTGRYLIST = "/getAllCtgries";
    public static final String UTIL_GET_CTGRY_NAME = "/getCtgryName/{id}";
    public static final String UTIL_GET_ALL_COLLLIST = "/getAllCollMthds";
    public static final String UTIL_GET_COLL_NAME = "/getCollMthdName/{id}";
    
} 