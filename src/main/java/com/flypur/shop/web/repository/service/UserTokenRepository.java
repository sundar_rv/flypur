package com.flypur.shop.web.repository.service;

import org.springframework.data.repository.CrudRepository;

import com.flypur.shop.web.repository.model.UserToken;

public interface UserTokenRepository extends CrudRepository<UserToken, String> {

}
