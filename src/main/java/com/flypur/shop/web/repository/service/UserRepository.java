package com.flypur.shop.web.repository.service;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.flypur.shop.web.repository.model.User;

public interface UserRepository extends PagingAndSortingRepository<User, String> {

    @Query("SELECT u FROM User u WHERE u.email = :email")
    User findByEmail(@Param("email") String email);
    
    @Query("SELECT u FROM User u WHERE u.loginId = :loginId")
    User findByLoginId(@Param("loginId") String loginId);
}
