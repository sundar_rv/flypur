package com.flypur.shop.web.repository.service;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.flypur.shop.web.repository.model.User;
import com.flypur.shop.web.repository.model.UserAddress;

public interface UserAddressRepository extends PagingAndSortingRepository<UserAddress, String> {

    @Query("SELECT u FROM UserAddress u WHERE u.city = :city")
    User findByCity(@Param("city") String city);
}
