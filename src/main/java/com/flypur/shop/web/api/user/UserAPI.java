package com.flypur.shop.web.api.user;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flypur.shop.config.AppConfig;
import com.flypur.shop.exception.ApplicationException;
import com.flypur.shop.mail.EmailService;
import com.flypur.shop.model.Mail;
import com.flypur.shop.util.APIUtil;
import com.flypur.shop.util.Constants;
import com.flypur.shop.util.DateUtil;
import com.flypur.shop.util.MD5Hash;
import com.flypur.shop.util.ReqMapConstants;
import com.flypur.shop.util.UniqueID;
import com.flypur.shop.web.api.response.APIStatus;
import com.flypur.shop.web.api.response.StatusResponse;
import com.flypur.shop.web.repository.model.User;
import com.flypur.shop.web.repository.model.UserAddress;
import com.flypur.shop.web.repository.model.UserToken;
import com.flypur.shop.web.service.UserService;
import com.flypur.shop.web.service.UserTokenService;

@RestController
@RequestMapping(ReqMapConstants.USERS)
public class UserAPI extends APIUtil {
	
	private static final Logger logger = LogManager.getLogger(UserAPI.class.getName());

    @Autowired
    private UserService userService;
    
    @Autowired
    private UserTokenService userTokenService;
    
    @Autowired
    private EmailService emailService;
    
    @Autowired
    private AppConfig appConfig;

    @RequestMapping(path = ReqMapConstants.USERS_REGISTER, 
    		method = RequestMethod.POST, 
    		produces = ReqMapConstants.CHARSET)
    public String register(@RequestBody User user) {
    	logger.debug("Transaction Name = User Registeration, " + user.getEmail());
        // check user already exists
        User existed = userService.getUserByEmail(user.getEmail());
        if (existed == null) {
            String email = user.getEmail(),
                    password = user.getPasswordHash();
            
            if (email != null && !email.equals("") && password != null && !password.equals("")) {

                Pattern pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(email);

                if (!matcher.matches()) {
                    statusResponse = new StatusResponse<Object>(APIStatus.ERR_INVALID_DATA);
                    return writeObjectToJson(statusResponse);
                } else if (password.length() < 6) {
                    statusResponse = new StatusResponse<Object>(APIStatus.PASS_INVALID);
                    return writeObjectToJson(statusResponse);
                }

                User userSignUp = new User();
                userSignUp.setLoginId(UniqueID.getUUID());
                userSignUp.setCreateDate(new Date());
                userSignUp.setEmail(email);
                userSignUp.setFirstName(user.getFirstName());
                userSignUp.setLastName(user.getLastName());
                userSignUp.setMiddleName(user.getMiddleName());
                userSignUp.setSalt(UniqueID.getUUID());

                try {
                    userSignUp.setPasswordHash(MD5Hash.MD5Encrypt(password + userSignUp.getSalt()));
                } catch (NoSuchAlgorithmException ex) {
                    throw new RuntimeException("Encrypt user password error", ex);
                }

                userSignUp.setRoleId(Constants.USER_ROLE.REGISTED_USER.getRoleId());
                userSignUp.setStatus(Constants.USER_STATUS.PENDING.getStatus());

                userService.save(userSignUp);
                sendMail(userSignUp);
                statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), userSignUp);
            } else {
                statusResponse = new StatusResponse<Object>(APIStatus.ERR_INVALID_DATA);
                return writeObjectToJson(statusResponse);
            }

        } else {
            statusResponse = new StatusResponse<Object>(APIStatus.USER_ALREADY_EXIST);
        }
        logger.debug("Transaction Name = User Registeration, Status = SUCCESS, " + user.getEmail());
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.USERS_ADD_ADDRESS, 
    		method = RequestMethod.POST, 
    		produces = ReqMapConstants.CHARSET)
    public String addAddress(@RequestBody UserAddress userAddress) {
    	logger.debug("Transaction Name = User Add Address, " + userAddress.getLoginId());
        // check user already exists
        User existed = userService.getUserByLoginId(userAddress.getLoginId());
        if (existed != null) {
            if (userAddress.getAddressLine1() != null) {
            	userService.save(userAddress);
                statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), userAddress);
            } else {
                statusResponse = new StatusResponse<Object>(APIStatus.ERR_INVALID_DATA);
                return writeObjectToJson(statusResponse);
            }
            logger.debug("Transaction Name = User Add Address, Status = SUCCESS, " + existed.getEmail());
        } else {
            statusResponse = new StatusResponse<Object>(APIStatus.USER_ALREADY_EXIST);
        }
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(path = ReqMapConstants.USERS_ACTIVATE_USER, 
    		method = RequestMethod.POST, 
    		produces = ReqMapConstants.CHARSET)
    public String activateUser(@PathVariable(value = "id") String loginId) {
    	logger.debug("Transaction Name = User Activate, " + loginId);
        // check user already exists
        User existed = userService.getUserByLoginId(loginId);
        if (existed != null) {
            if (existed.getStatus()==Constants.USER_STATUS.PENDING.getStatus() && validToken(existed.getCreateDate(), Constants.ONEDAY_MILLISEC)) {
            	existed.setStatus(Constants.USER_STATUS.ACTIVE.getStatus());
            	userService.save(existed);
                statusResponse = new StatusResponse<Object>(APIStatus.OK.getCode(), existed);
            } else {
                statusResponse = new StatusResponse<Object>(APIStatus.TOKEN_EXPIRIED);
                return writeObjectToJson(statusResponse);
            }
            logger.debug("Transaction Name = User Activate, Status = SUCCESS, " + existed.getEmail());
        } else {
            statusResponse = new StatusResponse<Object>(APIStatus.USER_DOESNT_EXIST);
        }
        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(value = ReqMapConstants.USERS_LOGIN, 
    		method = RequestMethod.POST, produces = ReqMapConstants.CHARSET)
    public String login(@RequestParam String email, @RequestParam String password, @RequestParam Boolean keepMeLogin) {

        if ("".equals(email) || "".equals(password)) {
            statusResponse = new StatusResponse<Object>(APIStatus.INVALID_PARAMETER);
        } else {
            User userLogin = userService.getUserByEmail(email);

            if (userLogin != null) {
                String passwordHash = null;
                try {
                    passwordHash = MD5Hash.MD5Encrypt(password + userLogin.getSalt());
                } catch (NoSuchAlgorithmException ex) {
                    throw new RuntimeException("User login encrypt password error", ex);
                }

                int userStatus = userLogin.getStatus();
                if (userStatus == Constants.USER_STATUS.ACTIVE.getStatus()) {
                    if (passwordHash.equals(userLogin.getPasswordHash())) {
                        UserToken userToken = new UserToken();
                        userToken.setToken(UniqueID.getUUID());
                        userToken.setLoginId(userLogin.getLoginId());

                        Date currentDate = new Date();
                        userToken.setLoginDate(DateUtil.convertToUTC(currentDate));

                        Date expirationDate = keepMeLogin ? new Date(currentDate.getTime() + Constants.ONEDAY_MILLISEC) : 
                        	new Date(currentDate.getTime() + Constants.THIRTYMIN_MILLISEC);
                        userToken.setExpirationDate(DateUtil.convertToUTC(expirationDate));

                        userTokenService.save(userToken);
                        statusResponse = new StatusResponse<>(HttpStatus.OK.value(), userToken);
                    } else {
                        // wrong password
                        statusResponse = new StatusResponse<Object>(APIStatus.ERR_USER_NOT_VALID);
                    }
                } else if (userStatus == Constants.USER_STATUS.PENDING.getStatus()) {
                    statusResponse = new StatusResponse<Object>(APIStatus.USER_PENDING_STATUS);
                } else {
                    statusResponse = new StatusResponse<Object>(APIStatus.ERR_USER_NOT_VALID);
                }
            } else {
                statusResponse = new StatusResponse<Object>(APIStatus.ERR_USER_NOT_EXIST);
            }
        }

        return writeObjectToJson(statusResponse);
    }
    
    @RequestMapping(value = ReqMapConstants.USERS_LOGIN, 
    		method = RequestMethod.GET, produces = ReqMapConstants.CHARSET)
    //Method to check everytime after login
    public String login(@RequestParam String tokenId) {

        if (tokenId == null) {
            statusResponse = new StatusResponse<Object>(APIStatus.INVALID_PARAMETER);
        } else {
        	UserToken token = userTokenService.getTokenById(tokenId);
            if (token != null) {
            	if(validToken(token.getExpirationDate(), new Long(0))) {
            		statusResponse = new StatusResponse<>(HttpStatus.OK.value(), token);
            	} else {
            		userTokenService.invalidateToken(token);
            		statusResponse = new StatusResponse<Object>(APIStatus.TOKEN_EXPIRIED);
            	}
            } else {
                statusResponse = new StatusResponse<Object>(APIStatus.TOKEN_EXPIRIED);
            }
        }
        return writeObjectToJson(statusResponse);
    }

    @RequestMapping(value = ReqMapConstants.USERS_LOGOUT, 
    		method = RequestMethod.POST, 
    		produces = ReqMapConstants.CHARSET)
    public String logout(@RequestParam String token) {

        UserToken userToken = userTokenService.getTokenById(token);
        if (userToken != null) {
            userTokenService.invalidateToken(userToken);
            statusResponse = new StatusResponse<Object>(APIStatus.OK);
        } else {
            statusResponse = new StatusResponse<Object>(APIStatus.INVALID_TOKEN);
        }

        return writeObjectToJson(statusResponse);
    }
    
    private boolean validToken(Date createDate, Long delay) {
    	Date validateDate = new Date(createDate.getTime() + delay);
    	Date now = DateUtil.convertToUTC(new Date());
    	if(now.before(validateDate)) {
    		return true;
    	} else {
    		return false;
    	}
	}

	private void sendMail(User user) {
    	try {
    		Map<String, Object> model = new HashMap<>();
    		model.put("name", user.getFirstName());
    		model.put("userEmail", user.getEmail());
    		model.put("loginId", user.getLoginId());
    		Mail mail = new Mail(user.getEmail(), appConfig.getValueOfKey("mail.subject.userRegister"), model);
            emailService.sendSimpleMessage(mail);
    	} catch (MessagingException | IOException ex) {
    		throw new ApplicationException(ex);
    	}
    }
}
