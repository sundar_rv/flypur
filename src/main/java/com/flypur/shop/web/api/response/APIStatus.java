package com.flypur.shop.web.api.response;

public enum APIStatus {

    ERR_INVALID_DATA(100, "Input data is not valid."),
    ERR_USER_NOT_EXIST(110, "User does not exist"),
    ERR_USER_NOT_VALID(111, "User name or password is not correct"),
    USER_ALREADY_EXIST(112, "Email already exist"),
    USER_PENDING_STATUS(113, "User have not activated"),
    USER_DOESNT_EXIST(114, "User doesn't exist"),
    INVALID_PARAMETER(200, "Invalid request parameter"),
    TOKEN_EXPIRIED(202, "Token expiried"),
    REQUIRED_LOGIN(203, "Required login"),
    INVALID_TOKEN(204, "Invalid token"),
    PASS_INVALID(205, "Password less than 6 digits"),
    // Common status
    OK(200, null),
    //Client Errors
	BAD(400, "Bad Request"),
	UNAUTH(401, "Unauthorized Request"),
	NOTFOUND(404, "Request not found"),
	//Server Errors
	NOTIMPL(501, "Request Not Implemented"),
	BADGTW(502, "Bad Gateway"),
	SVCUNAVAIL(503, "Service Unavailable"),
	GTWTIMOT(504, "Gateway Timeout");

    private final int code;
    private final String description;

    private APIStatus(int s, String v) {
        code = s;
        description = v;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}
