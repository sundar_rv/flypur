package com.flypur.shop.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flypur.shop.web.repository.model.User;
import com.flypur.shop.web.repository.model.UserAddress;
import com.flypur.shop.web.repository.service.UserAddressRepository;
import com.flypur.shop.web.repository.service.UserRepository;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserAddressRepository userAddressRepository;

    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }
    
    public User getUserByLoginId(String loginId) {
        return userRepository.findByLoginId(loginId);
    }

    public User save(User users) {
        return userRepository.save(users);
    }
    
    public UserAddress save(UserAddress userAddress) {
        return userAddressRepository.save(userAddress);
    }

    public User getUserById(String loginId) {
        return userRepository.findOne(loginId);
    }
    
    public UserAddress getUserAddressById(String loginId) {
        return userAddressRepository.findOne(loginId);
    }
}
