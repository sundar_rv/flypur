package com.flypur.shop.domain;

import java.io.Serializable;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.flypur.shop.util.LocalDateSerializer;

public class ManageOrder implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer orderId;
	
	private String loginId;
    
    private Status status;
    
    private CollectionMethod collectionMethod;
    
    private BuyDetails buyDetails;
    
    private TripDetails tripDetails;
    
    private String trackingNo;
    
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate expectedDelivery;
    
    public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public CollectionMethod getCollectionMethod() {
		return collectionMethod;
	}

	public void setCollectionMethod(CollectionMethod collectionMethod) {
		this.collectionMethod = collectionMethod;
	}

	public BuyDetails getBuyDetails() {
		return buyDetails;
	}

	public void setBuyDetails(BuyDetails buyDetails) {
		this.buyDetails = buyDetails;
	}

	public TripDetails getTripDetails() {
		return tripDetails;
	}

	public void setTripDetails(TripDetails tripDetails) {
		this.tripDetails = tripDetails;
	}

	public LocalDate getExpectedDelivery() {
		return expectedDelivery;
	}

	public void setExpectedDelivery(LocalDate expectedDelivery) {
		this.expectedDelivery = expectedDelivery;
	}

	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}

}
