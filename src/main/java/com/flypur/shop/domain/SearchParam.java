package com.flypur.shop.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class SearchParam {
	
	private List<KeyValue> keyValue;
	

	public SearchParam(String searchParamStr) {
		setSearchParam(searchParamStr);
	}

	public List<KeyValue> getKeyValue() {
		return keyValue;
	}

	public void setKeyValue(List<KeyValue> keyValue) {
		this.keyValue = keyValue;
	}
	
	public void setSearchParam(String searchParamStr) {
		StringTokenizer searchTokens = new StringTokenizer(searchParamStr, ",");
		keyValue = new ArrayList<>();
		while (searchTokens.hasMoreElements()) {
			String temp = searchTokens.nextToken();
			String[] keyValArr = temp.split("@@");
			if(keyValArr.length==3) {
				KeyValue keyVal = new KeyValue();
				keyVal.setKey(keyValArr[0]);
				keyVal.setOperation(keyValArr[1]);
				keyVal.setValue(keyValArr[2]);
				keyValue.add(keyVal);
			}
		}
	}
}
