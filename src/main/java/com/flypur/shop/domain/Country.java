package com.flypur.shop.domain;

public class Country {
	private String countryCde;
    
    private String countryName;

	public String getCountryCde() {
		return countryCde;
	}

	public void setCountryCde(String countryCde) {
		this.countryCde = countryCde;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
}
