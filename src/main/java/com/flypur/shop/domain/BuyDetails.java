package com.flypur.shop.domain;

import java.io.Serializable;
import java.util.List;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.flypur.shop.util.LocalDateSerializer;

public class BuyDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Integer buyId;
    
    private String loginId;
    
    private ProductDetails productDetails;
    private Integer quantity;
    
    private List<OfferDetails> offerDetails;
    
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate reqExpireDate;
    
    private Integer statusId;
    
    private String sendNotifications;
    
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate orderDate;

	public Integer getBuyId() {
		return buyId;
	}

	public void setBuyId(Integer buyId) {
		this.buyId = buyId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public LocalDate getReqExpireDate() {
		return reqExpireDate;
	}

	public void setReqExpireDate(LocalDate reqExpireDate) {
		this.reqExpireDate = reqExpireDate;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public String getSendNotifications() {
		return sendNotifications;
	}

	public void setSendNotifications(String sendNotifications) {
		this.sendNotifications = sendNotifications;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public ProductDetails getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(ProductDetails productDetails) {
		this.productDetails = productDetails;
	}

	public List<OfferDetails> getOfferDetails() {
		return offerDetails;
	}

	public void setOfferDetails(List<OfferDetails> offerDetails) {
		this.offerDetails = offerDetails;
	}

}
