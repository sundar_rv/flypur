package com.flypur.shop.domain;

import java.io.Serializable;

public class ProductCategory implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Integer categoryId;
    
	private String categoryName;
    
    private String subCategoryName;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

}
