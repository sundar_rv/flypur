package com.flypur.shop.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class OfferDetails implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer offerId; //aka tripId
	
	private Status status;
	
	private BigDecimal priceOffered;

	public Integer getOfferId() {
		return offerId;
	}

	public void setOfferId(Integer offerId) {
		this.offerId = offerId;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public BigDecimal getPriceOffered() {
		return priceOffered;
	}

	public void setPriceOffered(BigDecimal priceOffered) {
		this.priceOffered = priceOffered;
	} 

}
