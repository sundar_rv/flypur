package com.flypur.shop.domain;

import java.io.Serializable;

public class Flypur implements Serializable {

    private static final long serialVersionUID = 1L;
	
	private String transactionId;
	
	private String userToken;
	
	private TripDetails tripDetails;
	
	private BuyDetails buyDetails;
	
	private User user;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getUserToken() {
		return userToken;
	}

	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}

	public TripDetails getTripDetails() {
		return tripDetails;
	}

	public void setTripDetails(TripDetails tripDetails) {
		this.tripDetails = tripDetails;
	}

	public BuyDetails getBuyDetails() {
		return buyDetails;
	}

	public void setBuyDetails(BuyDetails buyDetails) {
		this.buyDetails = buyDetails;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}
