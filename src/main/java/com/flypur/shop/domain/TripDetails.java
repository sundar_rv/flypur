package com.flypur.shop.domain;

import java.io.Serializable;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.flypur.shop.util.LocalDateSerializer;

public class TripDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Integer tripId;
    
    private String loginId;
    
    private String countryCde;
    
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateRetrn;
    
    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate dateDlry;
    
    private Integer statusId;
    
    private String sendNotifications;

	public Integer getTripId() {
		return tripId;
	}

	public void setTripId(Integer tripId) {
		this.tripId = tripId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getCountryCde() {
		return countryCde;
	}

	public void setCountryCde(String countryCde) {
		this.countryCde = countryCde;
	}

	public LocalDate getDateRetrn() {
		return dateRetrn;
	}

	public void setDateRetrn(LocalDate dateRetrn) {
		this.dateRetrn = dateRetrn;
	}

	public LocalDate getDateDlry() {
		return dateDlry;
	}

	public void setDateDlry(LocalDate dateDlry) {
		this.dateDlry = dateDlry;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public String getSendNotifications() {
		return sendNotifications;
	}

	public void setSendNotifications(String sendNotifications) {
		this.sendNotifications = sendNotifications;
	}
}
