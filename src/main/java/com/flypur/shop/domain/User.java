package com.flypur.shop.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String loginId;
    
    private int roleId;
    
    private String email;
    
    private String passwordHash;
    
    private String firstName;
    
    private String middleName;
    
    private String lastName;
    
    private int status;
    
    private String salt;
    
    private Date createDate;
    
    private List<UserAddress> userAddressLst;
    
    public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public List<UserAddress> getUserAddressLst() {
		return userAddressLst;
	}
	
	public void setUserAddressLst(List<UserAddress> userAddressLst) {
		if(userAddressLst == null) {
			userAddressLst = new ArrayList<UserAddress>();
		}
		this.userAddressLst.addAll(userAddressLst);
	}

	public void addUserAddress(UserAddress userAddress) {
		if(userAddressLst == null) {
			userAddressLst = new ArrayList<UserAddress>();
		}
		this.userAddressLst.add(userAddress);
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}