package com.flypur.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flypur.shop.beanutils.UserBeanUtils;
import com.flypur.shop.domain.UserToken;
import com.flypur.shop.repository.model.UserTokenRepo;
import com.flypur.shop.repository.service.UserTokenRepository;

@Service
public class UserTokenService {

    @Autowired
    private UserTokenRepository userTokenRepository;

    public UserToken save(UserToken userToken) {
    	UserTokenRepo userTokenRepo = userTokenRepository.save(UserBeanUtils.getUserTokenRepo(userToken));
        return UserBeanUtils.getUserToken(userTokenRepo);
    }

    public UserToken getTokenById(String token) {
    	UserTokenRepo userTokenRepo = userTokenRepository.findOne(token);
        return UserBeanUtils.getUserToken(userTokenRepo);
    }

    public void invalidateToken(UserToken userToken) {
        userTokenRepository.delete(UserBeanUtils.getUserTokenRepo(userToken));
    }

}
