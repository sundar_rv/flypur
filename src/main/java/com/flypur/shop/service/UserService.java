package com.flypur.shop.service;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flypur.shop.beanutils.UserBeanUtils;
import com.flypur.shop.domain.User;
import com.flypur.shop.domain.UserAddress;
import com.flypur.shop.repository.model.UserAddressRepo;
import com.flypur.shop.repository.model.UserRepo;
import com.flypur.shop.repository.service.UserAddressRepository;
import com.flypur.shop.repository.service.UserRepository;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserAddressRepository userAddressRepository;
    
    public User saveUser(User user) {
    	UserRepo userRepo = userRepository.save(UserBeanUtils.getUserRepo(user));
    	List<UserAddressRepo> userAddressRepoLst = null;
    	if(CollectionUtils.isNotEmpty(user.getUserAddressLst())) {
    		userAddressRepoLst = UserBeanUtils.getUserAddressRepo(user);
    		for (UserAddressRepo userAddressRepo : userAddressRepoLst) {
    			userAddressRepo.setLoginId(userRepo.getLoginId());
    			userAddressRepository.save(userAddressRepo);
    		}
    	}
        return UserBeanUtils.getUser(userRepo, userAddressRepoLst);
    }
    
    public UserAddress saveUserAddress(UserAddress userAddress) {
    	UserAddressRepo userAddressRepo = userAddressRepository.save(UserBeanUtils.getUserAddressRepo(userAddress));
    	return UserBeanUtils.getUserAddress(userAddressRepo);
    }

    public User getUserByEmail(String email) {
    	UserRepo userRepo = userRepository.findByEmail(email);
    	List<UserAddressRepo> userAddressRepoLst = null;
    	if (userRepo!=null) {
    		userAddressRepoLst = userAddressRepository.findByLoginId(userRepo.getLoginId());
    	}
        return UserBeanUtils.getUser(userRepo, userAddressRepoLst);
    }
    
    public User getUserByLoginId(String loginId) {
    	UserRepo userRepo = userRepository.findByLoginId(loginId);
    	List<UserAddressRepo> userAddressRepoLst = userAddressRepository.findByLoginId(userRepo.getLoginId());
        return UserBeanUtils.getUser(userRepo, userAddressRepoLst);
    }
    
}
