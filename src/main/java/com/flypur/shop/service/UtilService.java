package com.flypur.shop.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flypur.shop.domain.CollectionMethod;
import com.flypur.shop.domain.Country;
import com.flypur.shop.domain.ProductCategory;
import com.flypur.shop.domain.Status;
import com.flypur.shop.repository.model.CollectionMethodRepo;
import com.flypur.shop.repository.model.CountryRepo;
import com.flypur.shop.repository.model.ProductCategoryRepo;
import com.flypur.shop.repository.model.StatusRepo;
import com.flypur.shop.repository.service.CollectionMethodRepository;
import com.flypur.shop.repository.service.CountryRepository;
import com.flypur.shop.repository.service.ProductCategoryRepository;
import com.flypur.shop.repository.service.StatusRepository;

@Service
public class UtilService {

    @Autowired
    private CountryRepository countryRepository;
    
    @Autowired
    private StatusRepository statusRepository;
    
    @Autowired
    private ProductCategoryRepository productCategoryRepository;
    
    @Autowired
    private CollectionMethodRepository collectionMethodRepository;

	public List<Country> getAllCountries() {
		List<Country> countryLst = new ArrayList<>();
		for (CountryRepo countryRepo : countryRepository.findAll()) {
			Country country = new Country();
			country.setCountryCde(countryRepo.getCountryCde());
			country.setCountryName(countryRepo.getCountryName());
			countryLst.add(country);
        }
		return countryLst;
	}

	public Country getCountryName(String countryCde) {
		CountryRepo countryRepo = countryRepository.findByCountryCde(countryCde);
		Country country = new Country();
		country.setCountryCde(countryRepo.getCountryCde());
		country.setCountryName(countryRepo.getCountryName());
		return country;
	}

	public List<Status> getAllStatus() {
		List<Status> statusLst = new ArrayList<>();
		for (StatusRepo statusRepo : statusRepository.findAll()) {
			Status status = new Status();
			status.setStatusId(statusRepo.getStatusId());
			status.setStatusName(statusRepo.getStatusName());
			statusLst.add(status);
        }
		return statusLst;
	}
	
	public Map<Integer, Status> getAllStatusMap() {
		Map<Integer, Status> statusMap = new HashMap<>();
		for (StatusRepo statusRepo : statusRepository.findAll()) {
			Status status = new Status();
			status.setStatusId(statusRepo.getStatusId());
			status.setStatusName(statusRepo.getStatusName());
			statusMap.put(status.getStatusId(), status);
        }
		return statusMap;
	}

	public Status getStatusName(Integer statusId) {
		StatusRepo statusRepo = statusRepository.findOne(statusId);
		Status status = new Status();
		status.setStatusId(statusRepo.getStatusId());
		status.setStatusName(statusRepo.getStatusName());
		return status;
	}

	public List<ProductCategory> getAllCategories() {
		List<ProductCategory> productCategoryLst = new ArrayList<>();
		for (ProductCategoryRepo productCategoryRepo : productCategoryRepository.findAll()) {
			ProductCategory productCategory = new ProductCategory();
			productCategory.setCategoryId(productCategoryRepo.getCategoryId());
			productCategory.setCategoryName(productCategoryRepo.getCategoryName());
			productCategory.setSubCategoryName(productCategoryRepo.getSubCategoryName());
			productCategoryLst.add(productCategory);
        }
		return productCategoryLst;
	}

	public ProductCategory getCategoryName(Integer categoryId) {
		ProductCategoryRepo productCategoryRepo = productCategoryRepository.findOne(categoryId);
		ProductCategory productCategory = new ProductCategory();
		productCategory.setCategoryId(productCategoryRepo.getCategoryId());
		productCategory.setCategoryName(productCategoryRepo.getCategoryName());
		productCategory.setSubCategoryName(productCategoryRepo.getSubCategoryName());
		return productCategory;
	}
	
	public List<CollectionMethod> getCollectionMethods() {
		List<CollectionMethod> collectionMethodLst = new ArrayList<>();
		for (CollectionMethodRepo collectionMethodRepo : collectionMethodRepository.findAll()) {
			CollectionMethod collectionMethod = new CollectionMethod();
			collectionMethod.setCollectionId(collectionMethodRepo.getCollectionId());
			collectionMethod.setMethodName(collectionMethodRepo.getMethodName());
			collectionMethodLst.add(collectionMethod);
		}
		return collectionMethodLst;
	}
	
	public CollectionMethod getCollectionMethodName(Integer collectionId) {
		CollectionMethodRepo collectionMethodRepo = collectionMethodRepository.findOne(collectionId);
		CollectionMethod collectionMethod = new CollectionMethod();
		collectionMethod.setCollectionId(collectionMethodRepo.getCollectionId());
		collectionMethod.setMethodName(collectionMethodRepo.getMethodName());
		return collectionMethod;
	}
}
