package com.flypur.shop.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.flypur.shop.beanutils.BuyDetailsBeanUtils;
import com.flypur.shop.beanutils.ProductDetailsBeanUtils;
import com.flypur.shop.domain.BuyDetails;
import com.flypur.shop.domain.SearchParam;
import com.flypur.shop.domain.Status;
import com.flypur.shop.repository.model.BuyDetailsRepo;
import com.flypur.shop.repository.model.CountryRepo;
import com.flypur.shop.repository.model.PreOrderRepo;
import com.flypur.shop.repository.model.ProductCategoryRepo;
import com.flypur.shop.repository.model.ProductDetailsRepo;
import com.flypur.shop.repository.service.BuyDetailsRepository;
import com.flypur.shop.repository.service.CountryRepository;
import com.flypur.shop.repository.service.PreOrderRepository;
import com.flypur.shop.repository.service.ProductCategoryRepository;
import com.flypur.shop.repository.service.ProductDetailsRepository;
import com.flypur.shop.repository.spec.SearchSpec;

@Service
public class BuyDetailService {
	
	@Autowired
    private BuyDetailsRepository buyDetailsRepository;
	
	@Autowired
    private ProductDetailsRepository productDetailsRepository;
	
	@Autowired
    private ProductCategoryRepository productCategoryRepository;
	
	@Autowired
    private CountryRepository countryRepository;
	
	@Autowired
	private PreOrderRepository preOrderRepository;
	
	@Autowired
	private UtilService utilService;
	
	public BuyDetails saveBuy(BuyDetails buyDetails) {
		BuyDetailsRepo buyDetailsRepo = BuyDetailsBeanUtils.getBuyDetailsRepo(buyDetails);
		ProductDetailsRepo productDetailsRepo = ProductDetailsBeanUtils.getProductDetailsRepo(buyDetails.getProductDetails());
		ProductCategoryRepo productCategoryRepo = productCategoryRepository.findByCategoryId(productDetailsRepo.getCategoryId());
		CountryRepo countryRepo = countryRepository.findByCountryCde(buyDetails.getProductDetails().getCountry().getCountryCde());
		productDetailsRepo = productDetailsRepository.save(productDetailsRepo);
		buyDetailsRepo.setProductId(productDetailsRepo.getProductId());
		buyDetailsRepo = buyDetailsRepository.save(buyDetailsRepo);
		List<PreOrderRepo> preOrderRepoLst = preOrderRepository.getOrderbyBuyId(buyDetails.getBuyId());
		Map<Integer, Status> statusMap = utilService.getAllStatusMap();
		return BuyDetailsBeanUtils.getBuyDetails(buyDetailsRepo, productDetailsRepo, productCategoryRepo, countryRepo, preOrderRepoLst, statusMap);
    }
	
	public void deleteBuy(BuyDetails buyDetails) {
    	BuyDetailsRepo buyDetailsRepo = BuyDetailsBeanUtils.getBuyDetailsRepo(buyDetails);
    	buyDetailsRepository.delete(buyDetailsRepo);
    }
	
	public BuyDetails getBuyById(Integer buyId) {
		BuyDetailsRepo buyDetailsRepo = buyDetailsRepository.findOne(buyId);
		if(buyDetailsRepo!=null) {
			return getBuyDetails(buyDetailsRepo);
		} else {
			return null;
		}
    }

	public BuyDetails getBuyById(String loginId, Integer buyId) {
		BuyDetailsRepo buyDetailsRepo = buyDetailsRepository.findByLoginIdBuyId(loginId, buyId);
		if(buyDetailsRepo!=null) {
			return getBuyDetails(buyDetailsRepo);
		} else {
			return null;
		}
    }

    public List<BuyDetails> getBuyByLoginId(String loginId) {
    	List<BuyDetailsRepo> buyDetailsRepoLst = buyDetailsRepository.findByLoginId(loginId);
    	return getBuyDetails(buyDetailsRepoLst);
    }

	public List<BuyDetails> getBuyByCountry(String loginId, String countryCde) {
    	List<BuyDetailsRepo> buyDetailsRepoLst = buyDetailsRepository.findBuyByCountry(loginId, countryCde);
    	return getBuyDetails(buyDetailsRepoLst);
    }

    public List<BuyDetails> searchBuyReq(SearchParam searchParam, String loginId){
    	Specification<BuyDetailsRepo> searchSpec = SearchSpec.buySearch(searchParam, loginId);
    	List<BuyDetailsRepo> buyDetailsRepo = buyDetailsRepository.findAll(searchSpec);
    	return getBuyDetails(buyDetailsRepo);
    }
    
    private BuyDetails getBuyDetails(BuyDetailsRepo buyDetailsRepo) {
		ProductDetailsRepo productDetailsRepo = productDetailsRepository.findByProductId(buyDetailsRepo.getProductId());
		ProductCategoryRepo productCategoryRepo = productCategoryRepository.findByCategoryId(productDetailsRepo.getCategoryId());
		CountryRepo countryRepo = countryRepository.findByCountryCde(productDetailsRepo.getCountryCde());
		List<PreOrderRepo> preOrderRepoLst = preOrderRepository.getOrderbyBuyId(buyDetailsRepo.getBuyId());
		Map<Integer, Status> statusMap = utilService.getAllStatusMap();
		return BuyDetailsBeanUtils.getBuyDetails(buyDetailsRepo, productDetailsRepo, productCategoryRepo, countryRepo, preOrderRepoLst, statusMap);
	}
    
    
    
    private List<BuyDetails> getBuyDetails(List<BuyDetailsRepo> buyDetailsRepoLst) {
		Map<Integer, ProductDetailsRepo> productDetailsRepoMap = new HashMap<>();
    	Map<Integer, ProductCategoryRepo> productCategoryRepoMap = new HashMap<>();
    	Map<String, CountryRepo> countryRepoMap = new HashMap<>();
    	Map<Integer, List<PreOrderRepo>> preOrderRepoMap = new HashMap<>();
    	Map<Integer, Status> statusMap = utilService.getAllStatusMap();
    	for (BuyDetailsRepo buyDetailsRepo : buyDetailsRepoLst) {
    		ProductDetailsRepo productDetailsRepo = productDetailsRepository.findByProductId(buyDetailsRepo.getProductId());
    		productDetailsRepoMap.put(buyDetailsRepo.getProductId(), productDetailsRepo);
    		ProductCategoryRepo productCategoryRepo = productCategoryRepository.findByCategoryId(productDetailsRepo.getCategoryId());
    		productCategoryRepoMap.put(productDetailsRepo.getCategoryId(), productCategoryRepo);
    		countryRepoMap.put(productDetailsRepo.getCountryCde(), countryRepository.findByCountryCde(productDetailsRepo.getCountryCde()));
    		List<PreOrderRepo> preOrderRepoLst = preOrderRepository.getOrderbyBuyId(buyDetailsRepo.getBuyId());
    		preOrderRepoMap.put(buyDetailsRepo.getBuyId(), preOrderRepoLst);
    	}    	
        return BuyDetailsBeanUtils.getBuyDetails(buyDetailsRepoLst, productDetailsRepoMap, 
        		productCategoryRepoMap, countryRepoMap, preOrderRepoMap, statusMap);
	}
    
}
