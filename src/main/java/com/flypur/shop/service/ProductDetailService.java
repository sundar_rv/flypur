package com.flypur.shop.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.flypur.shop.beanutils.ProductDetailsBeanUtils;
import com.flypur.shop.domain.ProductDetails;
import com.flypur.shop.domain.SearchParam;
import com.flypur.shop.repository.model.ProductCategoryRepo;
import com.flypur.shop.repository.model.ProductDetailsRepo;
import com.flypur.shop.repository.service.CountryRepository;
import com.flypur.shop.repository.service.ProductCategoryRepository;
import com.flypur.shop.repository.service.ProductDetailsRepository;
import com.flypur.shop.repository.spec.SearchSpec;

@Service
public class ProductDetailService {
	
	@Autowired
    private ProductDetailsRepository productDetailsRepository;
	
	@Autowired
    private ProductCategoryRepository productCategoryRepository;
	
	@Autowired
    private CountryRepository countryRepository;
	
	public List<ProductDetails> productSearch(SearchParam searchParam){
    	Specification<ProductDetailsRepo> searchSpec = SearchSpec.productSearch(searchParam);
    	List<ProductDetailsRepo> productDetailsRepo = productDetailsRepository.findAll(searchSpec);
    	return getProductDetails(productDetailsRepo);
    }
	
	private List<ProductDetails> getProductDetails(List<ProductDetailsRepo> productDetailsRepoLst) {
		List<ProductDetails> productDetailsLst = new ArrayList<>();
    	for (ProductDetailsRepo productDetailsRepo : productDetailsRepoLst) {
    		ProductCategoryRepo productCategoryRepo = productCategoryRepository.findByCategoryId(productDetailsRepo.getCategoryId());
    		ProductDetails productDetails = ProductDetailsBeanUtils.getProductsDetails(productDetailsRepo, productCategoryRepo, 
    				countryRepository.findByCountryCde(productDetailsRepo.getCountryCde()));
    		productDetailsLst.add(productDetails);
    	}    	
        return productDetailsLst;
	}

}
