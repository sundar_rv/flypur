package com.flypur.shop.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.flypur.shop.beanutils.ManageOrderBeanUtils;
import com.flypur.shop.config.AppConfig;
import com.flypur.shop.domain.BuyDetails;
import com.flypur.shop.domain.Mail;
import com.flypur.shop.domain.ManageOrder;
import com.flypur.shop.domain.OfferDetails;
import com.flypur.shop.domain.SearchParam;
import com.flypur.shop.domain.TripDetails;
import com.flypur.shop.domain.User;
import com.flypur.shop.exception.ApplicationException;
import com.flypur.shop.mail.EmailService;
import com.flypur.shop.repository.model.ManageOrderRepo;
import com.flypur.shop.repository.model.PreOrderRepo;
import com.flypur.shop.repository.service.ManageOrderRepository;
import com.flypur.shop.repository.service.PreOrderRepository;
import com.flypur.shop.repository.spec.SearchSpec;

@Service
public class ManageOrderService {
	
	@Autowired
	private ManageOrderRepository manageOrderRepository;
	
	@Autowired
	private PreOrderRepository preOrderRepository;
	
	@Autowired
	private BuyDetailService buyDetailService;
	
	@Autowired
	private TripDetailService tripDetailService;
	
	@Autowired
	private UtilService utilService;
	
	@Autowired
    protected EmailService emailService;
	
	@Autowired
	protected UserService userService;
	
	@Autowired
	protected AppConfig appConfig;
	
	private final static Logger logger = LogManager.getLogger(ManageOrderService.class.getName());
	
	public ManageOrder addPreOrder(ManageOrder manageOrder) {
		PreOrderRepo preOrderRepo = ManageOrderBeanUtils.getPreOrderRepo(manageOrder);
		preOrderRepo = preOrderRepository.save(preOrderRepo);
		manageOrder = getManageOrder(preOrderRepo, manageOrder);
		manageOrder.getBuyDetails().setStatusId(2);//Setting the status to Offered
		buyDetailService.saveBuy(manageOrder.getBuyDetails());
		User userT = userService.getUserByLoginId(manageOrder.getTripDetails().getLoginId());
		User userB = userService.getUserByLoginId(manageOrder.getBuyDetails().getLoginId());
		sendMail(userT, appConfig.getValueOfKey("mail.subject.orderOffer"), manageOrder, "orderOfferT");
		sendMail(userB, appConfig.getValueOfKey("mail.subject.orderOffer"), manageOrder, "orderOfferB");
		return manageOrder;
	}
	
	public ManageOrder updatePreOrderStatus(ManageOrder manageOrder) {
		PreOrderRepo preOrderRepo = ManageOrderBeanUtils.getPreOrderRepo(manageOrder);
		preOrderRepo = preOrderRepository.save(preOrderRepo);
		manageOrder = getManageOrder(preOrderRepo, manageOrder);
		String mailTemplete = null;
		if(manageOrder.getStatus().getStatusId() == 2) { //Offered
			manageOrder.getBuyDetails().setStatusId(2);//Setting the status to Offered
			mailTemplete = "orderOfferT";
			//Now create a record in actual order
			addOrder(manageOrder);
		} else if(manageOrder.getStatus().getStatusId() == 15) {//Declined
			manageOrder.getBuyDetails().setStatusId(1);//Setting the status to Open
			mailTemplete = "orderOfferDecliendT";
		} else if(manageOrder.getStatus().getStatusId() == 3) { //Accepted
			manageOrder.getBuyDetails().setStatusId(3);//Setting the status to Accepted
			mailTemplete = "orderOfferAcceptB";
			//Now create a record in actual order
			addOrder(manageOrder);
		} else if(manageOrder.getStatus().getStatusId() == 14) {//Rejected
			manageOrder.getBuyDetails().setStatusId(1);//Setting the status to Open
			mailTemplete = "orderOfferRejB";
		}
		buyDetailService.saveBuy(manageOrder.getBuyDetails());
		User userT = userService.getUserByLoginId(manageOrder.getTripDetails().getLoginId());
		sendMail(userT, appConfig.getValueOfKey("mail.subject.orderOffer"), manageOrder, mailTemplete);
		return manageOrder;
	}
	
	public boolean cancelOrderByTripBuy(TripDetails tripDetails, BuyDetails buyDetails) {
		boolean updated = false;
		cancelPreOrderByTripBuy(tripDetails, buyDetails);
		cancelManageOrderByTripBuy(tripDetails, buyDetails);
		return updated;
	}
	
	private boolean cancelPreOrderByTripBuy(TripDetails tripDetails, BuyDetails buyDetails) {
		boolean updated = false;
		if(tripDetails!=null) {
			if(tripDetails.getStatusId()==9) {//Trip Cancelled
				List<PreOrderRepo> preOrderRepoLst = preOrderRepository.getOrderbyTripId(tripDetails.getTripId());
				for(PreOrderRepo preOrderRepo : preOrderRepoLst) {
					preOrderRepo.setStatusId(tripDetails.getStatusId());//Order Cancelled
					preOrderRepo = preOrderRepository.save(preOrderRepo);
					ManageOrder manageOrder = getManageOrder(preOrderRepo, null);
					manageOrder.getBuyDetails().setStatusId(1);//Setting the status to Open
					buyDetailService.saveBuy(manageOrder.getBuyDetails());
				}
				updated = true;
			}
		}
		if(buyDetails!=null) {
			if(buyDetails.getStatusId()==9) {//buy Cancelled
				List<PreOrderRepo> preOrderRepoLst = preOrderRepository.getOrderbyBuyId(buyDetails.getBuyId());
				for(PreOrderRepo preOrderRepo : preOrderRepoLst) {
					preOrderRepo.setStatusId(buyDetails.getStatusId());//Order Cancelled
					preOrderRepo = preOrderRepository.save(preOrderRepo);
				}
				updated = true;
			}
			updated = true;
		}
		return updated;
	}
	
	private ManageOrder addOrder(ManageOrder manageOrder) {
		ManageOrderRepo manageOrderRepo = ManageOrderBeanUtils.getManageOrderRepo(manageOrder);
		manageOrder.getStatus().setStatusId(11);//Setting the status to Active
		manageOrderRepo = manageOrderRepository.save(manageOrderRepo);
		manageOrder = getManageOrder(manageOrderRepo, manageOrder);
		manageOrder.getBuyDetails().setStatusId(11);//Setting the status to Active
		buyDetailService.saveBuy(manageOrder.getBuyDetails());
		//here is where we need call payment gateway to get money from buy request
		return manageOrder;
	}

	public ManageOrder updateOrderStatus(ManageOrder manageOrder) {
		ManageOrderRepo manageOrderRepo = manageOrderRepository.findOne(manageOrder.getOrderId());
		manageOrderRepo.setStatusId(manageOrder.getStatus().getStatusId());
		manageOrderRepo = manageOrderRepository.save(manageOrderRepo);
		User userT = userService.getUserByLoginId(manageOrder.getTripDetails().getLoginId());
		User userB = userService.getUserByLoginId(manageOrder.getBuyDetails().getLoginId());
		manageOrderRepo.setStatusId(manageOrder.getStatus().getStatusId());
		sendMail(userT, appConfig.getValueOfKey("mail.subject.orderOffer"), manageOrder, "orderOrderStatusT");
		sendMail(userB, appConfig.getValueOfKey("mail.subject.orderOffer"), manageOrder, "orderOfferStatusB");
		manageOrder = getManageOrder(manageOrderRepo, manageOrder);
		
		return manageOrder;
	}
	
	public ManageOrder updateOrder(User user, ManageOrder manageOrder) {
		ManageOrderRepo manageOrderRepo = ManageOrderBeanUtils.getManageOrderRepo(manageOrder);
		manageOrderRepo = manageOrderRepository.save(manageOrderRepo);
		manageOrder = getManageOrder(manageOrderRepo, manageOrder);
		buyDetailService.saveBuy(manageOrder.getBuyDetails());
		
		
		sendMail(user, appConfig.getValueOfKey("mail.subject.orderDetails"), manageOrder, "");
		return manageOrder;
	}
	
	private boolean cancelManageOrderByTripBuy(TripDetails tripDetails, BuyDetails buyDetails) {
		boolean updated = false;
		if(tripDetails!=null) {
			if(tripDetails.getStatusId()==9) {//Trip Cancelled
				List<ManageOrderRepo> manageOrderRepoLst = manageOrderRepository.getOrderbyTripId(tripDetails.getTripId());
				for(ManageOrderRepo manageOrderRepo : manageOrderRepoLst) {
					manageOrderRepo.setStatusId(tripDetails.getStatusId());//Order Cancelled
					manageOrderRepo = manageOrderRepository.save(manageOrderRepo);
					ManageOrder manageOrder = getManageOrder(manageOrderRepo, null);
					manageOrder.getBuyDetails().setStatusId(1);//Setting the status to Open
					buyDetailService.saveBuy(manageOrder.getBuyDetails());
				}
				updated = true;
			}
		}
		if(buyDetails!=null) {
			if(buyDetails.getStatusId()==9) {//Buy Cancelled
				List<ManageOrderRepo> manageOrderRepoLst = manageOrderRepository.getOrderbyBuyId(buyDetails.getBuyId());
				for(ManageOrderRepo manageOrderRepo : manageOrderRepoLst) {
					manageOrderRepo.setStatusId(buyDetails.getStatusId());//Order Cancelled
					manageOrderRepo = manageOrderRepository.save(manageOrderRepo);
				}
				updated = true;
			}
			updated = true;
		}
		return updated;
	}
	
	public void removeOrder(User user, ManageOrder managerOrder) {
		manageOrderRepository.delete(ManageOrderBeanUtils.getManageOrderRepo(managerOrder));
		sendMail(user, appConfig.getValueOfKey("mail.subject.orderDetails"), managerOrder, "");
	}
	
	public void removeExpireOrder(User user, ManageOrder managerOrder) {
		manageOrderRepository.deleteExpireOrders(managerOrder.getBuyDetails().getBuyId());
	}
	
	public ManageOrder findOrder(Integer orderId) {
		ManageOrderRepo manageOrderRepo = manageOrderRepository.findOne(orderId);
		return getManageOrder(manageOrderRepo, null);
	}
	
	public ManageOrder findPreOrder(Integer orderId) {
		PreOrderRepo preOrderRepo = preOrderRepository.findOne(orderId);
		return getManageOrder(preOrderRepo, null);
	}
	
	public List<ManageOrder> findOrders(SearchParam searchParam) {
		List<ManageOrder> manageOrderlst = new ArrayList<>();
		Specification<ManageOrderRepo> searchSpec = SearchSpec.orderSearch(searchParam);
		List<ManageOrderRepo> manageOrderRepolst = manageOrderRepository.findAll(searchSpec);
		for (ManageOrderRepo manageOrderRepo : manageOrderRepolst) {
			ManageOrder manageOrder = getManageOrder(manageOrderRepo, null);
			manageOrderlst.add(manageOrder);
		}
		return manageOrderlst;
	}
	
	private ManageOrder getManageOrder(PreOrderRepo preOrderRepo, ManageOrder manageOrder) {
		ManageOrder managerOrder = ManageOrderBeanUtils.getPreOrder(preOrderRepo);
		managerOrder.setBuyDetails(buyDetailService.getBuyById(managerOrder.getBuyDetails().getBuyId()));
		managerOrder.setTripDetails(tripDetailService.getTripById(managerOrder.getTripDetails().getTripId()));
		managerOrder.setStatus(utilService.getStatusName(managerOrder.getStatus().getStatusId()));
		if(manageOrder!=null) {
			managerOrder.setLoginId(manageOrder.getLoginId());
		}
		return managerOrder;
	}
	
	private ManageOrder getManageOrder(ManageOrderRepo manageOrderRepo, ManageOrder manageOrder) {
		ManageOrder managerOrder = ManageOrderBeanUtils.getManageOrder(manageOrderRepo);
		managerOrder.setBuyDetails(buyDetailService.getBuyById(managerOrder.getBuyDetails().getBuyId()));
		managerOrder.setTripDetails(tripDetailService.getTripById(managerOrder.getTripDetails().getTripId(), 
				managerOrder.getLoginId()));
		managerOrder.setCollectionMethod(utilService.getCollectionMethodName(managerOrder.getCollectionMethod().getCollectionId()));
		managerOrder.setStatus(utilService.getStatusName(managerOrder.getStatus().getStatusId()));
		if(manageOrder!=null) {
			managerOrder.setLoginId(manageOrder.getLoginId());
		}
		return managerOrder;
	}
	
	private void sendMail(User user, String mailSubject, ManageOrder manageOrder, String template) {
    	try {
    		Map<String, Object> model = new HashMap<>();
    		model.put("name", user.getFirstName());
    		model.put("userEmail", user.getEmail());
    		model.put("loginId", user.getLoginId());
    		model.put("productName", manageOrder.getBuyDetails().getProductDetails().getProductName());
    		model.put("country", manageOrder.getBuyDetails().getProductDetails().getCountry().getCountryName());
    		String price= null;
    		for(OfferDetails offerDetails : manageOrder.getBuyDetails().getOfferDetails()) {
    			if(offerDetails.getOfferId().equals(manageOrder.getTripDetails().getTripId())) {
    				price = offerDetails.getPriceOffered().toString();
    			}
    		}
    		model.put("price", price);
    		Mail mail = new Mail(user.getEmail(), mailSubject, model);
            emailService.sendSimpleMessage(mail, template);
    	} catch (MessagingException | IOException ex) {
    		logger.error("ManageOrder - Error in sending email, " + ex.getMessage());
    		throw new ApplicationException(ex);
    	}
	}
	
}
