package com.flypur.shop.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flypur.shop.beanutils.TripDetailsBeanUtils;
import com.flypur.shop.domain.TripDetails;
import com.flypur.shop.repository.model.CountryRepo;
import com.flypur.shop.repository.model.TripDetailsRepo;
import com.flypur.shop.repository.service.CountryRepository;
import com.flypur.shop.repository.service.TripDetailsRepository;


@Service
public class TripDetailService {
	
	@Autowired
    private TripDetailsRepository tripDetailsRepository;
	
	@Autowired
    private CountryRepository countryRepository;
	
	@Autowired
	private ManageOrderService manageOrderService;
	
	public TripDetails saveTrip(TripDetails tripDetails) {
		TripDetailsRepo tripDetailsRepo = tripDetailsRepository.save(TripDetailsBeanUtils.getTripDetailsRepo(tripDetails));
		return TripDetailsBeanUtils.getTripDetails(tripDetailsRepo);
    }
	
	public TripDetails updateTrip(TripDetails tripDetails) {
		TripDetailsRepo tripDetailsRepo = TripDetailsBeanUtils.getTripDetailsRepo(tripDetails);
		if(tripDetails.getStatusId().equals(9)) {//If the Trip is cancelled, update the order if there is any
			manageOrderService.cancelOrderByTripBuy(tripDetails, null);
		}
		tripDetailsRepo = tripDetailsRepository.save(tripDetailsRepo);
		return TripDetailsBeanUtils.getTripDetails(tripDetailsRepo);
    }
	
	public TripDetails getTripById(Integer tripId) {
        return TripDetailsBeanUtils.getTripDetails(tripDetailsRepository.findOne(tripId));
    }
	
	public TripDetails getTripById(Integer tripId, String loginId) {
        return TripDetailsBeanUtils.getTripDetails(tripDetailsRepository.findByTripId(tripId, loginId));
    }

    public List<TripDetails> getTripByLoginId(String loginId) {
    	List<TripDetails> tripDetailsLst = new ArrayList<>();
    	List<TripDetailsRepo> tripDetailsRepoLst = tripDetailsRepository.findByLoginId(loginId);
    	for (TripDetailsRepo tripDetailsRepo : tripDetailsRepoLst) {
    		tripDetailsLst.add(TripDetailsBeanUtils.getTripDetails(tripDetailsRepo));
    	}
        return tripDetailsLst;
    }
    
    public List<TripDetails> getTripByCountry(String countryName, String loginId) {
    	CountryRepo country = countryRepository.findOne(countryName);        
        List<TripDetails> tripDetailsLst = new ArrayList<>();
        if(country!=null) {
        	List<TripDetailsRepo> tripDetailsRepoLst = tripDetailsRepository.findTripByCountry(country.getCountryCde(), loginId);
        	for (TripDetailsRepo tripDetailsRepo : tripDetailsRepoLst) {
        		tripDetailsLst.add(TripDetailsBeanUtils.getTripDetails(tripDetailsRepo));
        	}
        }
        return tripDetailsLst;
    }

    public void deleteTrip(TripDetails tripDetails) {
    	tripDetailsRepository.delete(TripDetailsBeanUtils.getTripDetailsRepo(tripDetails));
    }

}
